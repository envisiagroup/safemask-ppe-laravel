<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Site Root Routes
Route::get('/', function () { return view('front.home'); });
Route::get('/contact', function () { return view('front.contact'); });
Route::get('/privacy', function () { return view('front.privacy'); });
Route::get('/terms', function () { return view('front.terms'); });

// AKI1 Routes
Route::group(['prefix' => 'aki1'], function() {
    Route::get('/', function () { return view('aki1.home'); });
    Route::get('/contact', function () { return view('aki1.contact'); });
    Route::get('/privacy', function () { return view('aki1.privacy'); });
    Route::get('/terms', function () { return view('aki1.terms'); });
});

// RIP Routes
Route::group(['prefix' => 'rip'], function() {
    Route::get('/', function () { return view('rip.home'); });
    Route::get('/contact', function () { return view('rip.contact'); });
    Route::get('/privacy', function () { return view('rip.privacy'); });
    Route::get('/terms', function () { return view('rip.terms'); });
});
