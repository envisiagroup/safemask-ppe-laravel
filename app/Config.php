<?php

namespace App;

/**
 * Class Config
 * Stores Config keys as constants for easy access. Also serves as a repo of available config keys.
 * @package App
 */
class Config
{
    const SITE_ID = 'site_id';
    const API_TOKEN = 'api_token';
    const EMAIL = 'email';
    const USERNAME = 'username';
    const USER_ID = 'user_id';
    const IP = 'ip';
    const IP_IS_INTERNAL = 'ip_is_internal';
    const TEST_ORDER_CONFIG = 'test_order_config';
    const UUID = 'uuid';
    const I18N_ID = 'i18n_id';
    const COUNTRY_CODE = 'country_code';
    const LANGUAGE_CODE = 'language_code';
    const ORDER_TYPE = 'order_type';
    const PAYMENT_COMPLETE = 'payment_complete';
}
