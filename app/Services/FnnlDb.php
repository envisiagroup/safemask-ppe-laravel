<?php

namespace App\Services;

use Zttp\Zttp;

class FnnlDb
{
	private $token;

	private $baseUrl = 'https://api.fnnldb.com/v1';

	private $funnelId;

	public function __construct($funnelId = null)
	{
		$this->token = config('services.fnnldb.token');
		if(!$funnelId) {
			$this->funnelId = config('services.fnnldb.funnelId');
		} else {
			$this->funnelId = $funnelId;
		}
	}

	public function createLead($first_name, $last_name, $email, $phone, $address1, $address2 = null, $city, $state, $zip, $country, $billing_same, $billing_address1 = null, $billing_address2 = null, $billing_city = null, $billing_state = null, $billing_zip = null, $billing_country = null, $ip_address, $affiliate_id = null, $c1 = null, $c2 = null, $c3 = null)
	{
		$url = $this->baseUrl . '/leads/create';
		$data = [
			'key' => $this->token,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone' => $phone,
			'funnel_id' => $this->funnelId,
			'ip_address' => $ip_address,
			'address' => $address1,
			'address2' => $address2,
			'city' => $city,
			'state' => $state,
			'zipcode' => $zip,
			'country' => $country,
			'affiliate_id' => $affiliate_id,
			'sub_id' => $c1,
			'sub_id2' => $c2,
			'sub_id3' => $c3
		];

		$request = Zttp::asFormParams()->post($url, $data);
		return response()->json($request->body());
	}

	public function createOrder($first_name, $last_name, $email, $phone, $address1, $address2 = null, $city, $state, $zip, $country, $billing_same, $billing_address1 = null, $billing_address2 = null, $billing_city = null, $billing_state = null, $billing_zip = null, $billing_country = null, $card_number, $card_exp_month, $card_exp_year, $card_cvv, $products, $ip_address, $affiliate_id = null, $c1 = null, $c2 = null, $c3 = null)
	{
		$url = $this->baseUrl . '/orders/create';
		$data = [
			'key' => $this->token,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone' => $phone,
			'funnel_id' => $this->funnelId,
			'ip_address' => $ip_address,
			'address' => ($billing_same) ? $billing_address1 : $address1,
			'address2' => ($billing_same) ? $billing_address2 : $address2,
			'city' => ($billing_same) ? $billing_city : $city,
			'state' => ($billing_same) ? $billing_state : $state,
			'zipcode' => ($billing_same) ? $billing_zip : $zip,
			'country' => ($billing_same) ? $billing_country : $country,
			'shipping_first_name' => $first_name,
			'shipping_last_name' => $last_name,
			'shipping_address' => ($billing_same) ? $address1 : '',
			'shipping_address2' => ($billing_same) ? $address2 : '',
			'shipping_city' => ($billing_same) ? $city : '',
			'shipping_state' => ($billing_same) ? $state : '',
			'shipping_zipcode' => ($billing_same) ? $zip : '',
			'shipping_country' => ($billing_same) ? $country : '',
			'payment_source' => 'card',
			'card_number' => $card_number,
			'card_exp_month' => $card_exp_month,
			'card_exp_year' => $card_exp_year,
			'card_cvv' => $card_cvv,
			'products' => $products,
			'affiliate_id' => $affiliate_id,
			'sub_id' => $c1,
			'sub_id2' => $c2,
			'sub_id3' => $c3
		];

		$request = Zttp::asFormParams()->post($url, $data);

		return response()->json($request->body());
	}

	public function createOrderWithLead($leadId, $first_name, $last_name, $email, $phone, $address1, $address2 = null, $city, $state, $zip, $country, $billing_same, $billing_address1 = null, $billing_address2 = null, $billing_city = null, $billing_state = null, $billing_zip = null, $billing_country = null, $card_number, $card_exp_month, $card_exp_year, $card_cvv, $products, $ip_address, $affiliate_id = null, $c1 = null, $c2 = null, $c3 = null)
	{
		$url = $this->baseUrl . '/orders/create';
		$data = [
			'key' => $this->token,
			'lead_id' => $leadId,
			'first_name' => $first_name,
			'last_name' => $last_name,
			'email' => $email,
			'phone' => $phone,
			'funnel_id' => $this->funnelId,
			'ip_address' => $ip_address,
			'address' => ($billing_same) ? $billing_address1 : $address1,
			'address2' => ($billing_same) ? $billing_address2 : $address2,
			'city' => ($billing_same) ? $billing_city : $city,
			'state' => ($billing_same) ? $billing_state : $state,
			'zipcode' => ($billing_same) ? $billing_zip : $zip,
			'country' => ($billing_same) ? $billing_country : $country,
			'shipping_first_name' => $first_name,
			'shipping_last_name' => $last_name,
			'shipping_address' => ($billing_same) ? $address1 : '',
			'shipping_address2' => ($billing_same) ? $address2 : '',
			'shipping_city' => ($billing_same) ? $city : '',
			'shipping_state' => ($billing_same) ? $state : '',
			'shipping_zipcode' => ($billing_same) ? $zip : '',
			'shipping_country' => ($billing_same) ? $country : '',
			'payment_source' => 'card',
			'card_number' => $card_number,
			'card_exp_month' => $card_exp_month,
			'card_exp_year' => $card_exp_year,
			'card_cvv' => $card_cvv,
			'products' => $products,
			'affiliate_id' => $affiliate_id,
			'sub_id' => $c1,
			'sub_id2' => $c2,
			'sub_id3' => $c3
		];

		$request = Zttp::asFormParams()->post($url, $data);

		return response()->json($request->body());
	}

	public function createUpsell($orderId, $productId)
	{
		$url = $this->baseUrl . '/upsells/create';
		$data = [
			'key' => $this->token,
			'order_id' => $orderId,
			'product_id' => $productId,
		];

		$request = Zttp::asFormParams()->post($url, $data);
		return response()->json($request->body());
	}

	public function confirmOrder($orderId)
	{
		$url = $this->baseUrl . '/orders/confirm';
		$data = [
			'key' => $this->token,
			'order_id' => $orderId,
		];

		$request = Zttp::asFormParams()->post($url, $data);
		return response()->json($request->body());
	}
}
