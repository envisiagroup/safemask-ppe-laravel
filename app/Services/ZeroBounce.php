<?php

namespace App\Services;

use Zttp\Zttp;

class ZeroBounce
{
	protected $apiKey;

	public function __construct()
	{
		$this->apiKey = config('services.zerobounce.apiKey');
	}

	public function validate($email, $ip_address = null)
	{
		$url = 'https://api.zerobounce.net/v1/validate';

		$data = [
			'apikey' => $this->apiKey,
			'email' => urlencode($email)
		];

		if($ip_address) {
			$data['ipaddress'] = $ip_address;
			$url = 'https://api.zerobounce.net/v1/validatewithip';
		}

		$result = Zttp::post($url, $data);
		return $result->body();
	}
}
