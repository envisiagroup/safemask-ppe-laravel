<?php

namespace App\Services;

use Zttp\Zttp;

class Konnektive
{
    private $apiLoginId;

    private $apiPassword;

    private $baseUrl = 'https://api.konnektive.com';

    private $campaignId;

    public function __construct($campaignId = null)
    {
        $this->apiLoginId = config('services.konnektive.apiLoginId');
        $this->apiPassword = config('services.konnektive.apiPassword');
        if (!$campaignId) {
            $this->campaignId = config('services.konnektive.campaignId');
        } else {
            $this->campaignId = $campaignId;
        }
    }

    public function createLead($first_name, $last_name, $email, $phone, $address1, $address2 = null, $city, $state, $zip, $country, $billing_same, $billing_address1 = null, $billing_address2 = null, $billing_city = null, $billing_state = null, $billing_zip = null, $billing_country = null, $ip_address, $affiliate_id = null, $c1 = null, $c2 = null, $c3 = null)
    {
        $url = $this->baseUrl . '/leads/import/';
        $data = [
            'loginId' => $this->apiLoginId,
            'password' => $this->apiPassword,
            'firstName' => $first_name,
            'lastName' => $last_name,
            'emailAddress' => $email,
            'phoneNumber' => $phone,
            'billShipSame' => $billing_same,
            'campaignId' => $this->campaignId,
            'ipAddress' => $ip_address,
            'address1' => ($billing_same) ? $billing_address1 : $address1,
            'address2' => ($billing_same) ? $billing_address2 : $address2,
            'city' => ($billing_same) ? $billing_city : $city,
            'state' => ($billing_same) ? $billing_state : $state,
            'postalCode' => ($billing_same) ? $billing_zip : $zip,
            'country' => ($billing_same) ? $billing_country : $country,
            'shipFirstName' => $first_name,
            'shipLastName' => $last_name,
            'shipAddress1' => ($billing_same) ? $address1 : '',
            'shipAddress2' => ($billing_same) ? $address2 : '',
            'shipCity' => ($billing_same) ? $city : '',
            'shipState' => ($billing_same) ? $state : '',
            'shipPostalCode' => ($billing_same) ? $zip : '',
            'shipCountry' => ($billing_same) ? $country : '',
            'affId' => $affiliate_id,
            'sourceValue1' => $c1,
            'sourceValue2' => $c2,
            'sourceValue3' => $c3
        ];

        $request = Zttp::asFormParams()->post($url, $data);
        return response()->json($request->body());
    }

    public function createOrder($orderId, $customerId, $first_name, $last_name, $email, $phone, $address1, $address2 = null, $city, $state, $zip, $country, $billing_same, $billing_address1 = null, $billing_address2 = null, $billing_city = null, $billing_state = null, $billing_zip = null, $billing_country = null, $card_number, $card_exp_month, $card_exp_year, $card_cvv, $product_id, $ip_address, $affiliate_id = null, $c1 = null, $c2 = null, $c3 = null)
    {
        $url = $this->baseUrl . '/order/import/';
        $data = [
            'loginId' => $this->apiLoginId,
            'password' => $this->apiPassword,
            'orderId' => $orderId,
            'customerId' => $customerId,
            'firstName' => $first_name,
            'last_name' => $last_name,
            'emailAddress' => $email,
            'phoneNumber' => $phone,
            'billShipSame' => $billing_same,
            'campaignId' => $this->campaignId,
            'ipAddress' => $ip_address,
            'address1' => ($billing_same) ? $billing_address1 : $address1,
            'address2' => ($billing_same) ? $billing_address2 : $address2,
            'city' => ($billing_same) ? $billing_city : $city,
            'state' => ($billing_same) ? $billing_state : $state,
            'postalCode' => ($billing_same) ? $billing_zip : $zip,
            'country' => ($billing_same) ? $billing_country : $country,
            'shipFirstName' => $first_name,
            'shipLastName' => $last_name,
            'shipAddress1' => ($billing_same) ? $address1 : '',
            'shipAddress2' => ($billing_same) ? $address2 : '',
            'shipCity' => ($billing_same) ? $city : '',
            'shipState' => ($billing_same) ? $state : '',
            'shipPostalCode' => ($billing_same) ? $zip : '',
            'shipCountry' => ($billing_same) ? $country : '',
            'paySource' => 'CREDITCARD',
            'cardNumber' => $card_number,
            'cardMonth' => $card_exp_month,
            'cardYear' => $card_exp_year,
            'cardSecurityCode' => $card_cvv,
            'product1_id' => $product_id,
            'affId' => $affiliate_id,
            'sourceValue1' => $c1,
            'sourceValue2' => $c2,
            'sourceValue3' => $c3
        ];

        $request = Zttp::asFormParams()->post($url, $data);
        return response()->json($request->body());
    }

    public function createOrderWithManyProducts($orderId, $customerId, $first_name, $last_name, $email, $phone, $address1, $address2 = null, $city, $state, $zip, $country, $billing_same, $billing_address1 = null, $billing_address2 = null, $billing_city = null, $billing_state = null, $billing_zip = null, $billing_country = null, $card_number, $card_exp_month, $card_exp_year, $card_cvv, $products, $ip_address, $affiliate_id = null, $c1 = null, $c2 = null, $c3 = null)
    {
        $url = $this->baseUrl . '/order/import/';
        $data = [
            'loginId' => $this->apiLoginId,
            'password' => $this->apiPassword,
            'orderId' => $orderId,
            'customerId' => $customerId,
            'firstName' => $first_name,
            'last_name' => $last_name,
            'emailAddress' => $email,
            'phoneNumber' => $phone,
            'billShipSame' => $billing_same,
            'campaignId' => $this->campaignId,
            'ipAddress' => $ip_address,
            'address1' => ($billing_same) ? $billing_address1 : $address1,
            'address2' => ($billing_same) ? $billing_address2 : $address2,
            'city' => ($billing_same) ? $billing_city : $city,
            'state' => ($billing_same) ? $billing_state : $state,
            'postalCode' => ($billing_same) ? $billing_zip : $zip,
            'country' => ($billing_same) ? $billing_country : $country,
            'shipFirstName' => $first_name,
            'shipLastName' => $last_name,
            'shipAddress1' => ($billing_same) ? $address1 : '',
            'shipAddress2' => ($billing_same) ? $address2 : '',
            'shipCity' => ($billing_same) ? $city : '',
            'shipState' => ($billing_same) ? $state : '',
            'shipPostalCode' => ($billing_same) ? $zip : '',
            'shipCountry' => ($billing_same) ? $country : '',
            'paySource' => 'CREDITCARD',
            'cardNumber' => $card_number,
            'cardMonth' => $card_exp_month,
            'cardYear' => $card_exp_year,
            'cardSecurityCode' => $card_cvv,
            'affId' => $affiliate_id,
            'sourceValue1' => $c1,
            'sourceValue2' => $c2,
            'sourceValue3' => $c3
        ];

        $i = 1;
        foreach ($products as $product) {
            $data['product' . $i . '_id'] = $product->model->konnektive_id;
            $data['product' . $i . '_qty'] = $product->qty;
            $i++;
        }

        if ($products->count() == 1) {
            $data['product1_shipPrice'] = 4.95;
        }

        $request = Zttp::asFormParams()->post($url, $data);
        return response()->json($request->body());
    }

    public function createUpsell($orderId, $productId)
    {
        $url = $this->baseUrl . '/upsale/import/';
        $data = [
            'loginId' => $this->apiLoginId,
            'password' => $this->apiPassword,
            'orderId' => $orderId,
            'productId' => $productId,
        ];

        $request = Zttp::asFormParams()->post($url, $data);
        return response()->json($request->body());
    }

    public function confirmOrder($orderId)
    {
        $url = $this->baseUrl . '/order/confirm/';
        $data = [
            'loginId' => $this->apiLoginId,
            'password' => $this->apiPassword,
            'orderId' => $orderId,
        ];

        $request = Zttp::asFormParams()->post($url, $data);
        return response()->json($request->body());
    }

    public function getOrder($orderId)
    {
        $url = $this->baseUrl . '/order/query/';
        $data = [
            'loginId' => $this->apiLoginId,
            'password' => $this->apiPassword,
            'orderId' => $orderId,
        ];

        $request = Zttp::asFormParams()->post($url, $data);
        return response()->json($request->body());
    }
}
