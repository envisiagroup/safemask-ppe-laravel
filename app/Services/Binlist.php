<?php

namespace App\Services;

use Zttp\Zttp;

class Binlist
{
	public static function lookup($bin)
	{
		$response = Zttp::withHeaders(['Authorization' => 'Basic ' .  base64_encode(config('services.binlist.key')) . ':'])->get('https://lookup.binlist.net/' . $bin);
		return json_decode($response->body());
	}
}
