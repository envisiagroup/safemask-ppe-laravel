<?php

namespace App\Http\Middleware;

use App\Config;
use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class LoggerMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        // Generate & store UUID for current execution
        $uuid = uniqid() . 'xEG' . time();
        config([Config::UUID => $uuid]);

        // Determine IP Address
        if (env('APP_ENV') != 'production' && $request->hasHeader('X-CLIENT-IP')) {

            // Use test IP
            config([Config::IP => $request->header('X-CLIENT-IP')]);

        } else {

            // Use request IP
            config([Config::IP => $request->ip()]);
        }

        // Get DB
//        $db = app('db');

        if (preg_match('/(\.css|\.js)/i', $request->path())) {

            return $next($request);
        }

        // Create start header
        $start_header = "
 _______________________________________________________
|   __  ___       __  ___                               |
|  /__`  |   /\  |__)  |                                |
|  .__/  |  /~~\ |  \  |                                |
|_______________________________________________________|
";

        // Log execution start
        Log::info($start_header);

        // Create request array
        $request_array = [
            'path' => $request->path(),
            'method' => $request->getMethod(),
            'input' => $request->json()->all()
        ];

        // Log the request
        Log::info("REQUEST [METHOD]: " . $request_array['method']);
        Log::info("REQUEST [ROUTE]: " . $request_array['path']);
        Log::debug("REQUEST [DATA]: " . var_export($request_array['input'], true));

        // Get the response
        /** @var Response $response */
        $response = $next($request);

        // Create response array
        $response_array = [
            'status_code' => $response->getStatusCode(),
            'content' => $response->getContent()
        ];

        // Log the response data
        Log::info("RESPONSE [STATUS_CODE]: " . $response->getStatusCode());
//        Log::debug("RESPONSE [DATA]: " . $response->getContent());

        // Log SQL queries
        /*
        Log::debug("\n\n>>>>>>>>>>>>>>>>>>>>>> SQL QUERY DUMP >>>>>>>>>>>>>>>>>>>>>>\n\n");
        $queries = $db->getQueryLog();
        foreach ($queries as $query) {
            Log::debug($query['query'] . " (time: {$query['time']})", $query['bindings']);
        }

        // Get & dump queries from hfstore connection
        $queries = $db->connection('hfstore')->getQueryLog();
        foreach ($queries as $query) {
            Log::debug($query['query'] . " (time: {$query['time']})", $query['bindings']);
        }
        */

        // Calculate execution time
        $execution_time = round((microtime(true) - getenv('APP_START_TIME', true)) * 1000);

        // Save current request/response to api action log table
        /*
        $db->table('api_action_log')
            ->insert([
                'uuid' => config(Config::UUID),
                'user_id' => config(Config::USER_ID) ?: 0,
                'token' => $request->header('X-API-TOKEN'),
                'path' => $request->path(),
                'request' => json_encode($request_array),
                'response' => json_encode($response_array),
                'ip_address' => $request->ip(),
                'execution_time' => $execution_time,
                'created_datetime' => $db->raw('NOW()')
            ]);
        */

        // Return the response
        return $response->withHeaders([
            'X-API-UUID' => config(Config::UUID)
        ]);
    }
}
