<?php

namespace App\Http\Controllers;

use App\Mail\ContactMessage;
use TCG\Voyager\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function about()
    {
    	return view('front.about');
    }

    public function contact()
    {
    	return view('front.contact');
    }

    public function sendContact(Request $request)
    {
    	try {
    		Mail::to('hieroglyphics717@gmail.com')->send(new ContactMessage($request->name, $request->email, $request->subject, $request->message));
    		return redirect()->back()->with('success', 'message sent successfully');
    	} catch(\Exception $e) {
    		return redirect()->back()->with('error', $e->getMessage());
    	}
    }

    public function privacy()
    {
        $page = Page::where('slug', '=', 'privacy')->first();
        return view('front.page', ['page' => $page]);
    }

    public function terms()
    {
        $page = Page::where('slug', '=', 'terms')->first();
        return view('front.page', ['page' => $page]);
    }
}
