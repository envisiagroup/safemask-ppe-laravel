<?php

namespace App\Http\Controllers;

use App\SuppressedEmail;
use App\Services\ZeroBounce;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function validateEmail(Request $request)
    {
    	$data = $request->validate([
    		'email' => 'required|email'
    	]);

    	$zerobounce = new ZeroBounce;
    	$result = $zerobounce->validate($data['email']);
    	return $result;
    }

    public function download()
    {
    	$emails = SuppressedEmail::all();
    	$filename = "suppression_list.csv";
	    $handle = fopen($filename, 'w+');
	    fputcsv($handle, array('id', 'email', 'created at'));

	    foreach($emails as $email) {
	        fputcsv($handle, array($email->id, $email->email, $email->created_at));
	    }

	    fclose($handle);

	    $headers = array(
	        'Content-Type' => 'text/csv',
	    );

	    return response()->download($filename, 'suppression_list.csv', $headers);
    }

    public function unsubscribe(Request $request)
    {
    	$unsub = SuppressedEmail::create(['email' => $request->email]);
    	return redirect()->back()->with('success', 'success');
    }

    public function showUnsubscribe()
    {
    	return view('unsubscribe');
    }
}
