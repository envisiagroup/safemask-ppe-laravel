<?php

namespace App\Http\Controllers;

use Cart;
use Session;
use App\Services\FnnlDb;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function index()
    {
    	$cart_count = Cart::count();
    	if($cart_count == 0) {
    		return redirect('/cart');
    	} else {
    		$items = Cart::content();
    		$subtotal = Cart::subtotal();
    		$taxes = Cart::tax();
    		if($subtotal >= 100) {
    			$shipping = 0.00;
    		} else {
    			$shipping = 4.95;
    		}
    		$total = number_format(Cart::total() + $shipping, 2, '.', ',');
    		return view('checkout', ['cart_count' => $cart_count, 'items' => $items, 'subtotal' => $subtotal, 'taxes' => $taxes, 'shipping' => $shipping, 'total' => $total]);
    	}
    }

    public function checkout(Request $request)
    {
        if($request->billShipSame == 'yes') {
            $data = $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
                'zipcode' => 'required',
                'country' => 'required',
                'card_name' => 'required',
                'card_number' => 'required',
                'card_exp' => 'required',
                'card_cvv' => 'required',
            ]);
        } else {
            $data = $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                'phone' => 'required',
                'address' => 'required',
                'city' => 'required',
                'state' => 'required',
                'zipcode' => 'required',
                'country' => 'required',
                'billing_address' => 'required',
                'billing_city' => 'required',
                'billing_state' => 'required',
                'billing_zipcode' => 'required',
                'billing_country' => 'required',
                'card_name' => 'required',
                'card_number' => 'required',
                'card_exp' => 'required',
                'card_cvv' => 'required',
            ]);
        }

        $billing_address1 = ($request->billShipSame == 'yes') ? $data['address'] : $request->billing_address;
        $billing_address2 = ($request->billShipSame == 'yes') ? $request->address2 : $request->billing_address2;
        $billing_city = ($request->billShipSame == 'yes') ? $data['city'] : $request->billing_city;
        $billing_state = ($request->billShipSame == 'yes') ? $data['state'] : $request->billing_state;
        $billing_zipcode = ($request->billShipSame == 'yes') ? $data['zipcode'] : $request->billing_zipcode;
        $billing_country = ($request->billShipSame == 'yes') ? $data['country'] : $request->billing_country;

        $phone = str_replace('(', '', str_replace(')', '', str_replace('-', '', str_replace(' ', '', $data['phone']))));
        $card_number = str_replace(' ', '', $data['card_number']);
        $card_exp = explode('/', str_replace(' ', '', $data['card_exp']));
        $products = Cart::content();

        $products_arr = [];

        foreach($products as $product) {
            $item = [
                'id' => $product->model->fnnldb_id,
                'qty' => $product->qty
            ];
            array_push($products_arr, $item);
        }

        $fnnldb = new FnnlDb;

        $orderData = $fnnldb->createOrder($data['first_name'], $data['last_name'], $data['email'], $phone, $data['address'], $request->address2, $data['city'], $data['state'], $data['zipcode'], $data['country'], ($request->billShipSame) ? true : false, $billing_address1, $billing_address2, $billing_city, $billing_state, $billing_zipcode, $billing_country, $card_number, $card_exp[0], $card_exp[1], $data['card_cvv'], $products_arr, $request->ip(), '');
        $order = json_decode($orderData->getData());

        if($order && $order->status == 'success') {
            $fnnldb->confirmOrder($order->data->id);

            Session::put('order_' . $order->data->id, 'confirmed');
            Session::put('order_' . $order->data->id . '_items', Cart::content());
            Session::put('order_' . $order->data->id . '_total', Cart::total());

            Cart::destroy();

            return redirect('/checkout/complete/' . $order->data->id);
        } elseif($order && $order->status == 'error') {
            return redirect()->back()->withInput()->with('error', $order->data->error);
        } else {
            return redirect()->back()->withInput()->with('error', 'An unknown error occurred. Please try again.');
        }
    }

    public function complete($orderId, Request $request)
    {
        $items = session('order_' . $orderId . '_items');
        $total = session('order_' . $orderId . '_total');

        return view('checkout.complete', ['orderId' => $orderId, 'items' => $items, 'total' => $total]);
    }
}
