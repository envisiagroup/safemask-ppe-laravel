<?php

namespace App\Http\Controllers\Offers\SpecialOffer;

use Session;
use App\Services\Konnektive;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class V1Controller extends Controller
{
    public function index(Request $request)
    {
        if(session('main_product_qty')) {
            redirect('/offers/special-offer/receipt');
        }

        foreach($request->all() as $key => $value){
            Session::put($key, $value);
        }

    	return view('offers.special-offer.index');
    }

    public function checkout()
    {
        if(session('main_product_qty')) {
            redirect('/offers/special-offer/receipt');
        }
    	return view('offers.special-offer.checkout');
    }

    public function upsell1()
    {
        if((session('error') && session('error') == 'This upsale was already taken.') || (session('upsell1') && session('upsell1') == 'yes')) {
            return redirect('/offers/special-offer/upsell2');
        }

        return view('offers.special-offer.upsell1');
    }

    public function upsell2()
    {
        if((session('error') && session('error') == 'This upsale was already taken.') || (session('upsell2') && session('upsell2') == 'yes')) {
            return redirect('/offers/special-offer/upsell3');
        }

        return view('offers.special-offer.upsell2');
    }

    public function upsell3()
    {
        if((session('error') && session('error') == 'This upsale was already taken.') || (session('upsell3') && session('upsell3') == 'yes')) {
            return redirect('/offers/special-offer/receipt');
        }

        return view('offers.special-offer.upsell3');
    }

    public function receipt()
    {
        if(session('orderId')) {
            $konnektive = new Konnektive();
            $konnektive->confirmOrder(session('orderId'));
        }
        $main_product_qty = session('main_product_qty');
        $upsell1 = session('upsell1');
        $upsell2 = session('upsell2');
        $upsell3 = session('upsell3');
        $person = [
            'first_name' => session('first_name'),
            'last_name' => session('last_name'),
            'email' => session('email'),
            'phone' => session('phone'),
            'address' => session('address'),
            'city' => session('city'),
            'state' => session('state'),
            'zipcode' => session('zipcode'),
        ];
        Session::flush();
        return view('offers.special-offer.receipt', ['main_product_qty' => $main_product_qty, 'upsell1' => $upsell1, 'upsell2' => $upsell2, 'upsell3' => $upsell3, 'person' => $person]);
    }
}
