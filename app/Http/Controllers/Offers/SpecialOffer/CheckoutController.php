<?php

namespace App\Http\Controllers\Offers\SpecialOffer;

use Session;
use App\Throttle;
use App\Services\Binlist;
use App\Services\Konnektive;
use App\Services\ZeroBounce;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Setting;
use App\Http\Controllers\Controller;

class CheckoutController extends Controller
{
	public function createLead(Request $request)
	{
		$data = $request->validate([
	        'first_name' => 'required|max:255',
	        'last_name' => 'required|max:255',
	        'email' => 'required|email',
	        'phone' => 'required',
	        'address' => 'required|max:255',
	        'city' => 'required|max:255',
	        'state' => 'required',
	        'zipcode' => 'required|numeric',
	        'country' => 'required'
	    ]);

		if(config('services.zerobounce.apiKey')) {
			$zerobounce = new ZeroBounce;
	  		$validEmail = json_decode(substr($zerobounce->validate($data['email']), 0, -10));
	  		if($validEmail && $validEmail->status != 'Valid') {
	  			return redirect()->back()->withInput()->with('error', 'Please enter a valid email.');
	  		}
	  	}

		$phone = str_replace('(', '', str_replace(')', '', str_replace('-', '', str_replace(' ', '', $data['phone']))));
		$konnektive = new Konnektive(config('offers.special-offer.campaignId'));
	    $leadData = $konnektive->createLead($data['first_name'], $data['last_name'], $data['email'], $phone, $data['address'], '', $data['city'], $data['state'], $data['zipcode'], $data['country'], true, '', '', '', '', '', '', $request->ip(), session('affId'), session('c1'), session('c2'), session('c3'));
	    $lead = json_decode($leadData->getData());

	    if($lead && $lead->result == 'SUCCESS') {
	    	Session::put('customerId', $lead->message->customerId);
	    	foreach($data as $key => $value) {
	    		Session::put($key, $value);
	    	}

	    	return redirect($request->next_page);
	    } elseif($lead && $lead->result == 'ERROR') {
            return redirect()->back()->withInput()->with('error', $lead->message);
        } else {
            return redirect()->back()->withInput()->with('error', 'An unknown error occurred. Please try again.');
        }
	}

	public function createOrder(Request $request)
	{
		if(session('orderId') && session('main_product_qty')) {
			return redirect($request->next_page);
		}

		$data = $request->validate([
	        'card_number' => 'required',
	        'card_exp_month' => 'required',
	        'card_exp_year' => 'required',
	        'card_cvv' => 'required|numeric'
	    ]);

	    // check if card is prepaid
		$card_number = str_replace(' ', '', $data['card_number']);
		if(config('services.binlist.key')) {
			$bin = Binlist::lookup(substr($card_number, 0, 6));
			$is_prepaid = (isset($bin->prepaid)) ? $bin->prepaid : NULL;
		} else {
			$is_prepaid = null;
		}

		if(! $is_prepaid) {
			$campaignId = config('offers.special-offer.campaignId');
			$product_id = config('offers.special-offer.products.hero');
			$shipping_id = config('offers.special-offer.products.micro_trans');
			$redirect_session_header = 'first-upsell';
		} else {
			$campaignId = config('offers.special-offer-prepaid.campaignId');
			$product_id = config('offers.special-offer-prepaid.products.hero');
			$shipping_id = config('offers.special-offer-prepaid.products.micro_trans');
			$redirect_session_header = 'prepaid-upsell';
		}

		$phone = str_replace('(', '', str_replace(')', '', str_replace('-', '', str_replace(' ', '', session('phone')))));
		$billing_address1 = ($request->billingcheck == 'yes') ? session('address') : $request->billing_address;
		$billing_city = ($request->billingcheck == 'yes') ? session('city') : $request->billing_city;
		$billing_state = ($request->billingcheck == 'yes') ? session('state') : $request->billing_state;
		$billing_zipcode = ($request->billingcheck == 'yes') ? session('zipcode') : $request->billing_zipcode;
		$billing_country = ($request->billingcheck == 'yes') ? session('country') : $request->billing_country;

		$affId = session('affId');
		$c1 = session('c1');
		$c2 = session('c2');
		$c3 = session('c3');

		if(! $is_prepaid) {
			// get order count and internal percentage
			$internal_affiliate_id = config('envisia.site.internal_affiliate_id');
			$internal_percentage = (int) config('envisia.site.internal_percentage');
			$internal_order_count = (int) config('envisia.site.internal_order_count');
			$internal_order_count_next = $internal_order_count + 1;
			$konnektive_order_count = (int) config('envisia.site.konnektive_order_count');
			if($konnektive_order_count > 0) {
				if(session('affId')) {
					$throttle = Throttle::where('affiliate_id', '=', session('affId'))->first();
					if($throttle && $throttle->id) {
						if($throttle->percentage == 100) {
							$redirect_session_header = 'internal-upsell';
							$affId = $internal_affiliate_id;
							$c2 = session('affId');
						} else {
							$throttle_internal_count_next = $throttle->internal_count + 1;
							$throttle_order_count_percentage = number_format(($throttle_internal_count_next / $throttle->konnektive_count) * 100, 0, '.', '');
							if($throttle_order_count_percentage == $throttle->percentage) {
								$redirect_session_header = 'internal-upsell';
								$affId = $internal_affiliate_id;
								$c2 = session('affId');
							}
						}
					} else {
						$order_count_percentage = number_format(($internal_order_count_next / $konnektive_order_count) * 100, 0, '.', '');
						if($order_count_percentage == $internal_percentage) {
							$redirect_session_header = 'internal-upsell';
							$affId = $internal_affiliate_id;
							$c2 = session('affId');
						}
					}
				}
			}
		}

	    $konnektive = new Konnektive($campaignId);
	    $orderData = $konnektive->createOrder('', session('customerId'), session('first_name'), session('last_name'), session('email'), $phone, session('address'), '', session('city'), session('state'), session('zipcode'), session('country'), ($request->billingcheck) ? true : false, $billing_address1, '', $billing_city, $billing_state, $billing_zipcode, $billing_country, $card_number, $data['card_exp_month'], $data['card_exp_year'], $data['card_cvv'], $product_id, $request->ip(), $affId, $c1, $c2, $c3);
	    $order = json_decode($orderData->getData());

	    if($order && $order->result == 'SUCCESS') {
	    	Session::put('main_product_qty', $request->product_qty);
	    	Session::put('orderId', $order->message->orderId);
	    	Session::put('is_prepaid', $is_prepaid);
	    	Session::put('redirect_header', $redirect_session_header);

	    	// Micro Transaction
	    	if($request->has('micro_trans') && $request->micro_trans == 1) {
	    		$shippingCharge = $konnektive->createUpsell($order->message->orderId, $shipping_id);
	    	}

	    	if(! $is_prepaid) {
				if($affId != $internal_affiliate_id) {
					if(isset($throttle) && isset($throttle->id)) {
						$throttle->konnektive_count += 1;
						$throttle->save();
					} else {
						$oc = Setting::where('key', '=', 'site.konnektive_order_count')->first();
						$oc->value += 1;
						$oc->save();
					}
				} else {
					if(isset($throttle) && isset($throttle->id)) {
						$throttle->internal_count += 1;
						$throttle->save();
					} else {
						$oc = Setting::where('key', '=', 'site.internal_order_count')->first();
						$oc->value += 1;
						$oc->save();
					}
				}
			}

	    	return redirect($request->next_page)->with($redirect_session_header, number_format($request->product_qty * $request->product_price, 2, '.', ''));
	    } elseif($order && $order->result == 'ERROR') {
            return redirect()->back()->withInput()->with('error', $order->message);
        } else {
            return redirect()->back()->withInput()->with('error', 'An unknown error occurred. Please try again.');
        }
	}

	public function createUpsell(Request $request)
	{
		if(Session::has('upsell' . $request->upsell_id) && session('upsell' . $request->upsell_id) == 'yes') {
	    	return redirect($request->next_page);
	    }

		$product_name = $request->product_name;

		if(! session('is_prepaid')) {
			$campaignId = config('offers.special-offer.campaignId');
			$product_id = config('offers.special-offer.products.' . $product_name);
		} else {
			$campaignId = config('offers.special-offer-prepaid.campaignId');
			$product_id = config('offers.special-offer-prepaid.products.' . $product_name);
		}

		$konnektive = new Konnektive($campaignId);
		$orderData = $konnektive->createUpsell(session('orderId'), $product_id);
	    $order = json_decode($orderData->getData());

	    if($order && $order->result == 'SUCCESS') {
	    	Session::put('upsell' . $request->upsell_id, 'yes');
	    	return redirect($request->next_page);
	    } elseif($order && $order->status == 'ERROR') {
	    	$error = $order->message;
            return redirect($request->next_page)->with('error', $error);
        } else {
            return redirect($request->next_page)->with('error', 'An unknown error occurred. Please try again.');
        }
	}
}
