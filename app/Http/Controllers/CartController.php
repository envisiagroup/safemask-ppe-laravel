<?php

namespace App\Http\Controllers;

use Cart;
use Session;
use App\Coupon;
use App\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
    	$cart_count = Cart::count();
    	$items = Cart::content();
    	$subtotal = Cart::subtotal();
    	$taxes = Cart::tax();
    	$total = Cart::total();
    	return view('cart', ['cart_count' => $cart_count, 'items' => $items, 'subtotal' => $subtotal, 'taxes' => $taxes, 'total' => $total]);
    }

    public function add($productId)
    {
		$product = Product::find($productId);

		$item = Cart::add($product->id, $product->name, 1, $product->price);
		$item->associate('App\Product');

		return redirect('/cart');
    }

    public function update(Request $request)
    {
    	if($request->has('discount') && $request->discount != '') {
    		$discount = Coupon::where('code', '=', $request->discount)->first();
    	}
    	foreach($request->products as $product) {
    		if($product['qty'] > 0) {
    			Cart::update($product['id'], $product['qty']);
    		} else {
    			Cart::update($product['id'], 1);
    		}
    		if(isset($discount) && $discount->id) {
    			$cart = Cart::content();
    			$discount_total = 0;
    			foreach($cart as $item) {
    				if($discount->type == 'dollar') {
    					$newPrice = number_format($item->price - $discount->amount, 2, '.', '');
    					$discount_total += $discount->amount;
    				} elseif($discount->type == 'percent') {
    					$discount_amount = $item->price * ($discount->amount / 100);
    					$newPrice = number_format($item->price - $discount_amount, 2, '.', '');
    					$discount_total += $discount_amount;
    				}
    				Cart::update($item->rowId, ['price' => $newPrice]);
    			}
    			Session::put('discount_code', $discount->code);
    			Session::put('discount_total', $discount_total);
    		}
    	}
    	return redirect('/cart');
    }

    public function remove($rowId)
    {
    	Cart::remove($rowId);
    	return redirect('/cart');
    }
}
