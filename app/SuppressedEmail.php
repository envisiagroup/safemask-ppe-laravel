<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuppressedEmail extends Model
{
    protected $fillable = ['email'];
}
