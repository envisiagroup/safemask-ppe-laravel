<?php

namespace App\Logging;


use App\Config;
use Monolog\Logger;

class EnvisiaLoggingChannel
{
    public function __invoke(array $config)
    {
        // Create logger
        $monolog = new Logger('envisia-logging-channel');

        // Get process information
        $processUser = posix_getpwuid(posix_geteuid());
        $processName= $processUser[ 'name' ];

        // Set log file path
        $logPath = app()->storagePath() . config('LOG_FILE_PATH', '/logs/app-' . php_sapi_name() . '-' . $processName . '.log');

        // Create stream handler
        $logStreamHandler = new \Monolog\Handler\StreamHandler($logPath, env('APP_LOG_LEVEL', 100), true, null, true);

        // Create log formatter
        $formatter = new \Monolog\Formatter\LineFormatter("[%datetime%] [%uuid%] [%ip%] [%level_name%]: %message% %extra% %context%\n", null, true);
        $logStreamHandler->setFormatter($formatter);

        // Push stream handler to monolog
        $monolog->pushHandler($logStreamHandler);

        // Push processor to monolog
        $monolog->pushProcessor(function ($record) {

            // Sanitize message data
            $pattern = '/(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6011[0-9]{12}|3(?:0[0-5]|[68][0-9])[0-9]{11}|3[47][0-9]{13})(?![0-9]* secs)/';
            preg_match_all($pattern, $record['message'],$matches);

            // See if we have matches
            if (count($matches) !== 0) {

                // Loop matches
                foreach ($matches as $match)  {

                    // Loop match
                    foreach ($match as $str) {

                        // Trim string
                        $str = trim($str);

                        // Verify that it's an actual Card number
                        if (\Inacho\CreditCard::validCreditCard($str)) {

                            // Sanitize Card number
                            $replacement = \Envisia\Utils\CreditCardUtil::mask_cc_num(
                                $str,
                                \Envisia\Utils\CreditCardUtil::$MASK_MODE_LAST_FOUR,
                                null
                            );

                            // Update message
                            $record['message'] = str_replace($str, $replacement, $record['message']);
                        }
                    }
                }
            }

            // Replace membership key / encoding
            // TODO: figure out a better way to do this?
            $record['message'] = str_replace(env('MEMBERSHIP_KEY'), 'XXXXXXX', $record['message']);
            $record['message'] = str_replace('encode(', 'IFNULL(', $record['message']);
            $record['message'] = str_replace('decode(', 'IFNULL(', $record['message']);

            // Add data as needed
            $record['ip'] = config(Config::IP);
            $record['uuid'] = config(Config::UUID);
            $record['user_id'] = config(Config::USER_ID);

            // Return record
            return $record;
        });

        // Return monolog instance
        return $monolog;
    }
}
