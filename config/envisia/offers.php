<?php

return [
	'special-offer' => [
		'campaignId' => '4',
		'products' => [
			'hero' => '350',
			'step1' => '490',
			'step2' => '491',
			'micro_trans' => '',
		]
	],
	'special-offer-prepaid' => [
		'campaignId' => '4',
		'products' => [
			'hero' => '',
			'step1' => '',
			'step2' => '',
			'micro_trans' => '',
		]
	],
];
