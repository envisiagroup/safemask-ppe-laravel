<!DOCTYPE html>
<html lang="en" dir="ltr" class="js windows chrome page--no-banner page--logo-main page--show page--show card-fields cors svg opacity placeholder no-touchevents displaytable display-table generatedcontent cssanimations flexbox no-flexboxtweener anyflexbox no-shopemoji floating-labels">
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PCTCKQ3');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, height=device-height, minimum-scale=1.0, user-scalable=0">
    <meta name="robots" content="noindex, nofollow">
    <title>FDA Approved KN95 Face Masks Fast Shipping from USA</title>

    <!-- RK: Includes were changed to pull from public assets -->
    <link href="{{ asset('favicon.ico') }}" rel="icon" type="image/x-icon">
    <link href="{{ asset('css.css?family=PT+Serif:400,700&display=swap') }}" rel="stylesheet">
    <link rel="stylesheet" media="all" href="{{ asset('assets/dist/css/index.min.css') }}">

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="ns.html?id=GTM-PCTCKQ3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- RK: All Image includes were changed to pull from public assets -->
<img class="logo" src="{{ asset('assets/dist/images/logo.jpg') }}">
<div class="alert-note">
    <div class="container">
        <div class="red-box">
            <p class="alert-p1"><img class="alert-img" src="{{ asset('assets/dist/images/alert.gif') }}">Please Note:</p>
            <p class="alert-p2">We are experiencing <b>extremely high demand</b> for FDA Approved KN95 masks. If you order within the next <span class="timerStatic"></span> minutes, we guarantee arrival between:<br class="alert-br"> <b><span id="arrival"></span>.</b></p>
        </div>
    </div>
</div>

<div class="content">
    <div class="wrap">
        <div class="main" role="main">
            <div class="main__content">

                <div class="container">
                    <div class="inventory flex">
                        <img class="inventory-mask" src="{{ asset('assets/dist/images/mask.jpg') }}">
                        <div class="inventory-right">
                            <div class="flex">
                                <img class="inventory-flame" src="{{ asset('assets/dist/images/flame.gif') }}">
                                <p class="update">Inventory Update: We currently have only <span class="highlight"><span class="prod-left">236</span> Masks Left</span></p>
                            </div>
                            <div class="bar">
                                <div class="inner-bar"></div>
                            </div>
                            <div class="reserved desktop">Your Masks are Reserved for the Next - <span id="timer">0:00</span></div>
                        </div>
                    </div>
                    <div class="reserved mobile">Your Masks are Reserved for the Next:<br> <span id="timer-mobile">0:00</span></div>
                </div>

                <form id="payment_form" name="payment_form" method="post" action="/cart/checkout/cart/process" onsubmit="setCCExp(); updateccName();">
                    <input type="hidden" name="isSubmit" value="true">
                    <input type="hidden" name="cartUniqueID" value="">
                    <input type="hidden" name="sID" value="288">
                    <input type="hidden" name="siteID" value="288">
                    <input type="hidden" name="cShipping" id="cShipping" value="10.00">
                    <input type="hidden" name="cShipMethod" id="cShipMethod" value="Standard Shipping">
                    <input type="hidden" name="shipping" id="shippingCombined" value="10.00|Standard Shipping">
                    <input type="hidden" name="cID" id="cID" value="">
                    <input type="hidden" name="offer_id" value="453">
                    <input type="hidden" name="back" value="https://www.safemaskppe.com/">
                    <input type="hidden" name="m" value="add">
                    <input type="hidden" name="cRefer" value="https://www.safemaskppe.com/">
                    <input type="hidden" name="tpt_canvas" value="fHx1aWR4fHM6NDI6IlBQMjAyMDA3MjExNDQ4MTl1aGZzNzFxN2ljdTcydHE1MHJhanNhNTFvbiI7fHxjdXN0b21fcGFyYW1zfGE6NDp7czoxOiJyIjtzOjI4OiJodHRwczovL3d3dy5zYWZlbWFza3BwZS5jb20vIjtzOjM6ImNpcCI7czoxMjoiNDUuMjEuMTc0LjE0IjtzOjY6InNlcnZlciI7czoxOToid3d3LnNhZmVtYXNrcHBlLmNvbSI7czo4OiJwYWdlX3VybCI7czoxOiIvIjt9fHw=">
                    <input type="hidden" name="fgpid" value="">
                    <input type="hidden" name="pQty" value="1">
                    <input type="hidden" name="path_id" value="92">
                    <input type="hidden" name="checkout_type" value="TWO_PAGE_CHECKOUT">
                    <input type="hidden" name="cShipCountry" value="United States">
                    <input type="hidden" name="cBillCountry" value="United States">
                    <input type="hidden" name="variant" value="root">
                    <input type="hidden" name="ccExp" id="ccExp" value="">
                    <input type="hidden" name="ccType" id="ccType" value="">
                    <input type="hidden" name="ccName" id="ccName" value="">
                    <input type="hidden" name="ccName_type" value="hidden">
                    <input type="hidden" name="ccType_view" id="ccType_view" value="hidden">
                    <input type="hidden" name="cCode" id="cCode" value="">
                    <input type="hidden" name="allow_cardTypes" id="allow_cardTypes" value="Visa,MasterCard,Discover,American Express">
                    <input type="hidden" name="allow_landing_in_cart" value="Y">
                    <input type="hidden" id="shipping_validation" name="shipping_validation" value="Y">
                    <input type="hidden" name="allowShippingOverwrite" value="Y">
                    <input type="hidden" name="static_index_exit" value="Y">

                    <div class="selection">
                        <div class="container">
                            <table>
                                <thead>
                                <tr>
                                    <th colspan="2" class="qty-select-head"><b>Select Quantity</b></th>
                                    <th class="price-cell"><b>Price</b></th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="container">
                        <div class="bonus">
                            <div class="top">Now Available for a Limited Time!</div>
                            <div class="body">
                                <div class="flex">
                                    <img class="mask" src="{{ asset('assets/dist/images/mask-face.jpg') }}">
                                    <div class="">
                                        <img class="rating" src="{{ asset('assets/dist/images/rating.gif') }}">
                                        <p class="header"><b>Reusable Face Mask Adjustable Ear Protector Hook</b></p>
                                        <p class="description desktop">Adjustable ear hook strap to help relieve discomfort, pressure and pain associated with face mask ear elastics</p>
                                    </div>
                                </div>
                                <p class="description mobile">Adjustable ear hook strap to help relieve discomfort, pressure and pain associated with face mask ear elastics</p>
                            </div>
                            <div class="bottom"><input type="checkbox" name="hook-add"> Yes, Add 5 for $5 to my order</div>
                        </div>
                    </div>

                    <div class="order-summ mobile">
                        <div class="header">
                            <p><img class="cart-icon" src="{{ asset('assets/dist/images/cart-icon.gif') }}">Order Summary</p>
                        </div>
                        <div class="container">
                            <table>
                                <tr>
                                    <td><span class="mask-amount">10</span> Official KN95 Face Masks</td>
                                    <td class="right">$<span class="subtotal-price">0.00</span></td>
                                </tr>
                                <tr class="hook-row">
                                    <td>Face Make Adjustable Ear Hook</td>
                                    <td class="right">$5.00</td>
                                </tr>
                                <tr class="shipping-row">
                                    <td>Shipping and Handling</td>
                                    <td class="right shipping-price">$10.00</td>
                                </tr>
                                <tr class="total-row">
                                    <td>Total</td>
                                    <td class="total-wrapper right">$<span class="total-price">0.00</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div class="summary-seals mobile">
                        <img class="seals" src="{{ asset('assets/dist/images/seals.png') }}">
                    </div>

                    <hr class="hr desktop">

                    <div class="customer-info">
                        <div class="container">
                            <section class="info-group section section--contact-information">
                                <div class="section__header">
                                    <h1>1. Shipping Information</h1>
                                    <h2>Contact information</h2>
                                </div>
                                <div class="section__content">
                                    <div class="fieldset">
                                        <div class="field field--required">
                                            <label class="field__label" for="checkout_email">Email</label>
                                            <div class="field__input-wrapper">
                                                <input placeholder="Email" autocomplete="shipping email" aria-required="true" class="field__input" size="30" type="email" name="cEmail" id="cEmail" value="" onblur="tempData.customer.email = $(this).val(); sendProspect(prospectData);" required="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>




                            <!-- SHIPPING -->
                            <section class="info-group section section--shipping-address">

                                <div class="section__header">
                                    <h2 class="section__title">Shipping address</h2>
                                </div>

                                <div class="section__content">

                                    <div class="fieldset">
                                        <div>
                                            <div class="field--half field field--optional">
                                                <label class="field__label" for="cShipFname">First name</label>
                                                <div class="field__input-wrapper">
                                                    <input placeholder="First name" autocomplete="shipping given-name" autocorrect="off" class="field__input" size="30" type="text" name="cShipFname" onblur="tempData.customer.first_name = $(this).val(); tempData.shipping_address.first_name = $(this).val(); sendProspect(prospectData);" id="cShipFname" value="" required/="">
                                                </div>
                                            </div>
                                            <div class="field--half field field--required">
                                                <label class="field__label" for="cShipLname">Last name</label>
                                                <div class="field__input-wrapper">
                                                    <input placeholder="Last name" autocomplete="shipping family-name" autocorrect="off" class="field__input" aria-required="true" size="30" type="text" name="cShipLname" onblur="tempData.customer.last_name = $(this).val(); tempData.shipping_address.last_name = $(this).val();" id="cShipLname" value="" required/="">
                                                </div>
                                            </div>

                                            <div class="field field--required">
                                                <label class="field__label" for="checkout_shipping_address_address1">Address</label>
                                                <div class="field__input-wrapper">
                                                    <input placeholder="Address" autocomplete="shipping address-line1" autocorrect="off" role="combobox" aria-autocomplete="list" aria-owns="address-autocomplete" aria-expanded="false" aria-required="true" class="field__input" size="30" type="text" name="cShipAddress1" onblur="tempData.shipping_address.line_1 = $(this).val();" id="cShipAddress1" value="" required="">
                                                </div>
                                            </div>

                                            <div class="field--third field field--required">
                                                <label class="field__label" for="checkout_shipping_address_zip">Zip</label>
                                                <div class="field__input-wrapper">
                                                    <input placeholder="Zip" autocomplete="shipping postal-code" autocorrect="off" class="field__input field__input--zip" aria-required="true" maxlength="5" type="tel" name="cShipZip" onblur="tempData.shipping_address.zip = $(this).val();" id="cShipZip" value="" required/="">
                                                </div>
                                            </div>

                                            <div class="field--third field field--required">
                                                <label class="field__label" for="checkout_shipping_address_city">City</label>
                                                <div class="field__input-wrapper">
                                                    <div id="city_wrap">
                                                        <input placeholder="City" class="field__input" type="text" name="cShipCity" onblur="tempData.shipping_address.city = $(this).val();" id="cShipCity" value="" required="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="field--third field field--required">
                                                <label class="field__label" for="cShipState">State</label>
                                                <select name="cShipState" id="province" class="field__input" onblur="tempData.shipping_address.state = $(this).val();" required="">
                                                    <option value="" selected="selected">State</option>

                                                    <option value="AL">Alabama</option>
                                                    <option value="AK">Alaska</option>
                                                    <option value="AZ">Arizona</option>
                                                    <option value="AR">Arkansas</option>
                                                    <option value="CA">California</option>
                                                    <option value="CO">Colorado</option>
                                                    <option value="CT">Connecticut</option>
                                                    <option value="DE">Delaware</option>
                                                    <option value="DC">District of Columbia</option>
                                                    <option value="FL">Florida</option>
                                                    <option value="GA">Georgia</option>
                                                    <option value="HI">Hawaii</option>
                                                    <option value="ID">Idaho</option>
                                                    <option value="IL">Illinois</option>
                                                    <option value="IN">Indiana</option>
                                                    <option value="IA">Iowa</option>
                                                    <option value="KS">Kansas</option>
                                                    <option value="KY">Kentucky</option>
                                                    <option value="LA">Louisiana</option>
                                                    <option value="ME">Maine</option>
                                                    <option value="MD">Maryland</option>
                                                    <option value="MA">Massachusetts</option>
                                                    <option value="MI">Michigan</option>
                                                    <option value="MN">Minnesota</option>
                                                    <option value="MS">Mississippi</option>
                                                    <option value="MO">Missouri</option>
                                                    <option value="MT">Montana</option>
                                                    <option value="NE">Nebraska</option>
                                                    <option value="NV">Nevada</option>
                                                    <option value="NH">New Hampshire</option>
                                                    <option value="NJ">New Jersey</option>
                                                    <option value="NM">New Mexico</option>
                                                    <option value="NY">New York</option>
                                                    <option value="NC">North Carolina</option>
                                                    <option value="ND">North Dakota</option>
                                                    <option value="OH">Ohio</option>
                                                    <option value="OK">Oklahoma</option>
                                                    <option value="OR">Oregon</option>
                                                    <option value="PA">Pennsylvania</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="RI">Rhode Island</option>
                                                    <option value="SD">South Dakota</option>
                                                    <option value="SC">South Carolina</option>
                                                    <option value="TN">Tennessee</option>
                                                    <option value="TX">Texas</option>
                                                    <option value="UT">Utah</option>
                                                    <option value="VT">Vermont</option>
                                                    <option value="VA">Virginia</option>
                                                    <option value="WA">Washington</option>
                                                    <option value="WV">West Virginia</option>
                                                    <option value="WI">Wisconsin</option>
                                                    <option value="WY">Wyoming</option>
                                                </select>
                                            </div>

                                            <div class="field field--required">
                                                <label class="field__label" for="checkout_shipping_address_city">Phone</label>
                                                <div class="field__input-wrapper">
                                                    <input placeholder="Phone" autocomplete="shipping address-level2" autocorrect="off" class="field__input" aria-required="true" size="30" type="text" name="cPhone2" onblur="tempData.shipping_address.city = $('#cShipCity').val(); tempData.shipping_address.state = $('#province').val(); tempData.customer.phone_1 = $(this).val(); sendProspect(prospectData);" id="cPhone2" value="" required="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- END SHIPPING -->


                            <!-- BILLING -->
                            <section class="info-group section section--billing-address">
                                <h1>2. Billing Information</h1>
                                <h2>Payment</h2>
                                <p class="secure-text"><img class="shield-img" src="{{ asset('assets/dist/images/check-shield.gif') }}"> All transactions are 100% secure and encrypted</p>

                                <div class="cc-wrapper">
                                    <div class="header flex">
                                        <p>Credit Card</p>
                                        <img class="cc-img" src="{{ asset('assets/dist/images/cards.png') }}">
                                    </div>
                                    <div class="body">
                                        <div class="field field--required">
                                            <label class="field__label" for="checkout_shipping_address_city">Card Number</label>
                                            <div class="field__input-wrapper">
                                                <input placeholder="Credit Card" maxlength="16" autocomplete="shipping address-level2" autocorrect="off" class="field__input" aria-required="true" size="30" type="tel" name="ccNum" required="">
                                            </div>
                                        </div>

                                        <div class="exp-wrapper">
                                            <div class="field--half field field--required field--mo">
                                                <div class="field__input-wrapper field__input-wrapper--select">
                                                    <select class="field__input field__input--select" id="Expmonth" name="Expmonth" onchange="setCCExp(); combineExpDate();" required="">
                                                        <option selected="" disabled="" value="">Month</option>
                                                        <option value="01">01</option>
                                                        <option value="02">02</option>
                                                        <option value="03">03</option>
                                                        <option value="04">04</option>
                                                        <option value="05">05</option>
                                                        <option value="06">06</option>
                                                        <option value="07">07</option>
                                                        <option value="08">08</option>
                                                        <option value="09">09</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="field--half field field--required field--yr">
                                                <div class="field__input-wrapper field__input-wrapper--select">
                                                    <select class="field__input field__input--select" id="Expyear" name="Expyear" onchange="setCCExp(); combineExpDate();" required=""></select>
                                                </div>
                                            </div>
                                            <input type="hidden" id="ccExpValidation" name="ccExpValidation" value="">
                                        </div>


                                        <div class="field--half field field--required">
                                            <input class="field__input" size="30" type="tel" id="ccCode" name="ccCode" maxlength="4" placeholder="Security code" required="">
                                        </div>

                                        <div class="cvv-tooltip field--half field field--required">
                                            <div class="field__input-wrapper field__input-wrapper--icon-right">
                                                <div id="credit_card_verification_value_tooltip" role="tooltip" class="field__icon has-tooltip">
                                                        <span id="tooltip-for-verification_value" class="tooltip">
                                                            <span>
                                                                3-digit security code usually found on the back of your card. American Express cards have a 4-digit code located on the front.
                                                            </span>
                                                        </span>
                                                    What is this?
                                                </div>
                                            </div>
                                        </div>



                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>



                                <div class="section__header billing">
                                    <h2 class="section__title">Billing address</h2>
                                </div>

                                <div class="ship-btns billship same-ship"> <input type="radio" name="billEqualShip" value="Y" checked="">Same as shipping address</div>
                                <div class="diff-ship-wrapper">
                                    <div class="ship-btns billship diff-ship"><input type="radio" name="billEqualShip" value="">Use a different billing address</div>
                                    <div class="billing-info section__content">
                                        <div class="fieldset">
                                            <div>
                                                <div class="field--half field field--optional">
                                                    <label class="field__label" for="cBillFname">First name</label>
                                                    <div class="field__input-wrapper">
                                                        <input placeholder="First name" autocomplete="shipping given-name" autocorrect="off" class="field__input" size="30" type="text" name="cBillFname" id="cBillFname" value="" required="">
                                                    </div>
                                                </div>
                                                <div class="field--half field field--required">
                                                    <label class="field__label" for="cBillLname">Last name</label>
                                                    <div class="field__input-wrapper">
                                                        <input placeholder="Last name" autocomplete="shipping family-name" autocorrect="off" class="field__input" aria-required="true" size="30" type="text" name="cBillLname" id="cBillLname" value="" required="">
                                                    </div>
                                                </div>

                                                <div class="field field--required">
                                                    <label class="field__label" for="checkout_shipping_address_address1">Address</label>
                                                    <div class="field__input-wrapper">
                                                        <input placeholder="Address" autocomplete="shipping address-line1" autocorrect="off" role="combobox" aria-autocomplete="list" aria-owns="address-autocomplete" aria-expanded="false" aria-required="true" class="field__input" size="30" type="text" name="cBillAddress1" id="cBillAddress1" value="" required="">
                                                    </div>
                                                </div>

                                                <div class="field--third field field--required">
                                                    <label class="field__label" for="checkout_shipping_address_zip">Zip</label>
                                                    <div class="field__input-wrapper">
                                                        <input placeholder="Zip" autocomplete="shipping postal-code" autocorrect="off" class="field__input field__input--zip" maxlength="5" type="tel" name="cBillZip" id="cBillZip" value="" required="">
                                                    </div>
                                                </div>

                                                <div class="field--third field field--required">
                                                    <label class="field__label" for="checkout_shipping_address_city">City</label>
                                                    <div class="field__input-wrapper">
                                                        <div id="city_wrap_billing">
                                                            <input placeholder="City" class="field__input" type="text" name="cBillCity" id="cBillCity" value="" required="">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="field--third field field--required">
                                                    <label class="field__label" for="cShipState">State</label>
                                                    <select id="cBillState" name="cBillState" class="field__input" required="">
                                                        <option value="" selected="selected">State</option>

                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="DC">District of Columbia</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="PR">Puerto Rico</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>


                                            </div>


                                        </div>

                                    </div>
                                </div>
                            </section>
                            <!-- END BILLING -->


                            <div class="">
                                <button type="submit" class="submit btn">Complete My Order</button>
                            </div>
                            <img class="form-seals" src="{{ asset('assets/dist/images/footer-imgs.png') }}" alt="">

                            <div class="footer desktop">
                                <ul>
                                    <li><a href="javascript:void(0);" onclick="openPopUpUrl('rip/contact', 700, 1110)">Contact US</a></li>
                                    <li> | </li>
                                    <li><a href="javascript:void(0);" onclick="openPopUpUrl('rip/privacy', 700, 1110)">Privacy Policy</a></li>
                                    <li> | </li>
                                    <li><a href="javascript:void(0);" onclick="openPopUpUrl('rip/terms', 700, 1110)">Terms and Conditions</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="sidebar" role="complementary">
            <div class="sidebar__content">
                <div id="order-summary" class="order-summary order-summary--is-collapsed">
                    <div class="container">
                        <h2 class="visually-hidden-if-js">Order summary</h2>

                        <div class="order-summary__sections">
                            <img class="seals-d" src="{{ asset('assets/dist/images/seals.png') }}" alt="">
                            <hr class="hr">
                            <table>
                                <thead>
                                <tr>
                                    <th>Order Summary</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="main-prod"><span class="mask-amount">10</span> Official KN95 Face Masks</td>
                                    <td class="right">$<span class="subtotal-price">0.00</span></td>
                                </tr>
                                <tr class="hook-row">
                                    <td>Face Mask Adjustable Ear Hook</td>
                                    <td class="right">$5.00</td>
                                </tr>
                                <tr class="shipping-row">
                                    <td>Shipping &amp; Handling</td>
                                    <td class="shipping-price right">$10.00</td>
                                </tr>
                                <tr class="total-row">
                                    <td>Total:</td>
                                    <td class="total-wrapper right">$<span class="total-price">0.00</span></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="testimonials">
                    <div class="container">
                        <hr class="mobile">
                        <h2>Customer Testimonials</h2>
                        <div class="review review1">
                            <div class="flex">
                                <img class="person" src="{{ asset('assets/dist/images/p1.jpg') }}">
                                <div class="right">
                                    <img class="rating" src="{{ asset('assets/dist/images/rating.gif') }}">
                                    <img class="verified" src="{{ asset('assets/dist/images/verified.gif') }}">
                                    <span class="name">Rebecca A.</span>
                                </div>
                            </div>
                            <p class="quote">These are the most comfortable ones I have ever used! They came quick as promised and I feel super safe knowing these are the official FDA approved ones. Do not get ripped off and pay for overpriced, knock-off masks that most places try to shell out.</p>
                        </div>
                        <div class="review review2">
                            <div class="flex">
                                <img class="person" src="{{ asset('assets/dist/images/p2.jpg') }}">
                                <div class="right">
                                    <img class="rating" src="{{ asset('assets/dist/images/rating.gif') }}">
                                    <img class="verified" src="{{ asset('assets/dist/images/verified.gif') }}">
                                    <span class="name">Nolan K.</span>
                                </div>
                            </div>
                            <p class="quote">This is your life and my life we talking about here. I did the research and it’s scary how much junk is being sold out there. Do yourself and your loved ones a favor and get these exact ones from SafeMaskPPE.com. They guarantee delivery so you WILL get the authentic KN95 masks fast.</p>
                        </div>
                        <div class="review review3">
                            <div class="flex">
                                <img class="person" src="{{ asset('assets/dist/images/p3.jpg') }}">
                                <div class="right">
                                    <img class="rating" src="{{ asset('assets/dist/images/rating.gif') }}">
                                    <img class="verified" src="{{ asset('assets/dist/images/verified.gif') }}">
                                    <span class="name">Courtney M.</span>
                                </div>
                            </div>
                            <p class="quote">Your search is over. Authentic KN95 masks with FDA approval at these great prices...?! You just wish more companies would provide service like this one. Oh and the ear protector hook thing really was nice to have! My whole family wears these to stay safe.</p>
                        </div>
                        <hr>
                    </div>
                </div>

                <div class="faqs">
                    <div class="container">
                        <h2>FAQ</h2>
                        <div class="question-wrapper">
                            <p class="title">1. When will my order ship?</p>
                            <p class="answer">You are guaranteed delivery with no last minute cancellations. Once it is reserved, they are yours. After you place your order you will receive a tracking number within 2-3 days.</p>
                        </div>
                        <div class="question-wrapper">
                            <p class="title">2. Where does my order ship from?</p>
                            <p class="answer">Your order will come from our own facility in Hollywood, FL USA. We are following every guideline stated by the CDC and ensuring every square inch is thoroughly sanitized on a daily basis.</p>
                        </div>
                        <div class="question-wrapper">
                            <p class="title">3. What certifications do these masks have?</p>
                            <p class="answer">This mask has been granted an official certification by the FDA in 2020. This mask has also been CE Certified in 2020.</p>
                        </div>
                        <div class="question-wrapper">
                            <p class="title">4. Are they as good as N95 masks?</p>
                            <p class="answer">Absolutely. KN95 masks are certified to filter at least 95% of airborne particles > 0.3 microns. Which is equivalent to the N95 mask.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- RK: removed ".html" to match routes -->
<div class="footer mobile">
    <ul>
        <li><a href="javascript:void(0);" onclick="openPopUpUrl('rip/contact', 700, 1110)">Contact US</a></li>
        <li> | </li>
        <li><a href="javascript:void(0);" onclick="openPopUpUrl('rip/privacy', 700, 1110)">Privacy Policy</a></li>
        <li> | </li>
        <li><a href="javascript:void(0);" onclick="openPopUpUrl('rip/terms', 700, 1110)">Terms and Conditions</a></li>
    </ul>
</div>

<!-- RK: Changed to Global CDN instead of local includes -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js" integrity="sha512-DaXizW7rneJiM/XOnTQVQ7wDZBVNXf5U9rE88BPYhQcEQ4pjaEZlCX5hgY3+4C3K91jfdpUWbz8t8mL/g1BDTw==" crossorigin="anonymous"></script>
<script>
    var coupon = "";
    var ver = "";

    // Form Submit
    function submit(){
        if($('.billship').find('input[name="billEqualShip"]').val() == "Y"){
            tempData.billing_address.first_name = $('#cShipFname').val();
            tempData.billing_address.last_name = $('#cShipLname').val();
            tempData.billing_address.line_1 = $('#cShipAddress1').val();
            tempData.billing_address.city = $('#cShipCity').val();
            tempData.billing_address.state = $('#province').val();
            tempData.billing_address.zip = $('#cShipZip').val();
        } else {
            tempData.billing_address.first_name = $('#cBillFname').val();
            tempData.billing_address.last_name = $('#cBillLname').val();
            tempData.billing_address.line_1 = $('#cBillAddress1').val();
            tempData.billing_address.city = $('#cBillCity').val();
            tempData.billing_address.state = $('#cBillState').val();
            tempData.billing_address.zip = $('#cBillZip').val();
        }
        sendProspect(prospectData);

        setTimeout(function(){
            document.payment_form.submit();
        }, 600);
    }
</script>
<script src="{{ asset('assets/dist/js/index.min.js') }}"></script>
<script src="{{ asset('index.js?format=jsonp&callback=getIP') }}"></script>

</body>
</html>
