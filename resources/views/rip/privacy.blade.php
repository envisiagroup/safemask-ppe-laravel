
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PCTCKQ3');</script>
<!-- End Google Tag Manager -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Privacy Policy | SafeMaskPPE</title>
    <meta name="robots" content="noindex, nofollow">
    <link href="//d2e2sm5hsxwj7z.cloudfront.net/aki1/dist/css/popup.min.css" rel="stylesheet" type="text/css" />
    <link href="//www.safemaskppe.com/favicon.ico" rel="icon" type="image/x-icon">
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCTCKQ3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	<header>
    <h1 align="center"><strong>PRIVACY POLICY</strong></h1>
</header>
<main>
    <section>
        <div class="container">
            <br>
            <p>Effective Date: January 15, 2016</p>

            <p>First Logic LLC (“The Company”) owns and operates the website <a href="https://www.safemaskppe.com">www.safemaskppe.com</a> and all subdomains (collectively, “Website”) as well as the online business over the Website. All references to “we”, “us”, this “Website” or this “Site” means The Company and this website business.</p>

            <h2>HOW WE MODIFY THIS PRIVACY POLICY</h2>
            <p>We reserve the right to modify this Privacy Policy at any time, and without prior notice, by posting an amended Privacy Policy that is always accessible by clicking on the “Privacy Policy” link on this Website’s home page (<a href="https://www.safemaskppe.com/privacy">http://www.safemaskppe.com/privacy</a>). You should check back periodically for updates and modifications to the Privacy Policy.  Your continued use of this Website after the amended Privacy Policy is posted indicates your acceptance of the amended Privacy Policy.</p>

            <p>Regarding Personal Information (defined below), if any modifications are materially less restrictive on our use or disclosure of the Personal Information previously disclosed by you, we will notify you before implementing such revisions with respect to such information as to you.</p>

            <h2>THE TYPES OF INFORMATION WE COLLECT</h2>
            <p>Personal Information. “Personal Information” includes any information regarding a natural person that may be used directly to identify the person. Personal Information that we collect may vary with each separate purpose for which you provide it, and it may include one or more of the following categories: name, physical address, an email address, phone number, credit card information, including credit card number, expiration date, and billing address, and certain location data.</p>

            <p>Usage Data. We reserve the right to collect information based on your usage of this Website which is information collected automatically from this Website (or third party services employed in connection with this Website), which can include: websites visited prior to visiting this Website, the IP addresses or domain names of the computers utilized by the users who use this Website, the URI addresses (Uniform Resource Identifier), the time of the request to the server, the method utilized to submit the request to the server, the size of the file received in response, the numerical code indicating the status of the server’s answer (successful outcome, error, etc.), the country of origin, the features of the browser and the operating system utilized by the user, the various time details per visit (e.g., the time spent on each page within the Website) and the details about the path followed within the Website with special reference to the sequence of pages visited, other parameters about the device operating system and/or the user’s IT environment, and data, conversion rates, marketing and conversion data and statistics, reports, analytics data, reviews and surveys (collectively, “Usage Data”). Usage Data is essentially anonymous when collected, but could be used indirectly to identify a person.</p>

            <h2>HOW AND WHEN WE COLLECT INFORMATION</h2>
            <p>Personal Information. We collect Personal Information at the time you provide it to us. We collect Personal Information through sign-up forms and as part of your purchase or registration for an account, product, or service, promotion, or contest from this Website. Personal Information that we collect may vary with each sign-up, purchase or registration. In addition, we collect Personal Information from all communications with Website visitors including without limitation, text messages, e-mails, faxes, telephone calls, and regular “snail mail”, as well as from third-party outside sources including database vendors.</p>

            <p>Your Communications With Us. We collect Personal Information that we receive from you as you communicate with us. If you complete, in whole or in part, a form on our Website, attempt to, or actually order a product or service, signup to our Website or to participate in our mailing list activities, we will receive your Personal Information from our Website services, email services and/or telecommunications service.</p>

            <p>Usage Data. We reserve the right to monitor your use of this Website. As you navigate through this Website, Usage Data may be passively collected (that is, gathered without your actively providing the information) using various analytics and reporting technologies, such as cookies and web beacons.</p>

            <h2>HOW WE USE YOUR INFORMATION</h2>
            <p>We may use your Personal Information for the performance of the services or transaction for which it was given, and in connection with other products, services, promotions, or contests we may offer, and our private, internal reporting for this Website, and security assessments for this Website.  Without limiting the foregoing, Personal Information may be used to communicate with you regarding your registration for any Website service and purchase or attempt to purchase any product or service including, but not limited to, sending you e-mail, text messages, and other communications regarding any incomplete order.  Personal Information may also be used to send you information about healthy living, diets, and physical fitness, including e-mail newsletters, from our third-party wellness partners.  See the “E-MAIL MARKETING” section below for information about how to opt out of receiving these and other e-mails.</p>

            <p>We reserve the right to make full use of Usage Data. For example, we may use Usage Data to provide better service to Website visitors, customize the Website based on your preferences, compile and analyze statistics and trends about the use of this Website, and otherwise administer and improve this Website for your use. Specific uses are described below.</p>

            <h2>INFORMATION SHARING AND DISCLOSURE</h2>
            <p>General Disclosure Policy. We reserve the right to disclose your Personal Information as described below. We reserve the right to disclose Usage Data without restriction.</p>

            <p>Affiliated Entities. We reserve the right to provide your Personal Information and Usage Data to any of our affiliated entities, including our subsidiaries. Affiliated entities are entities that we legally control (by voting rights) or that control us.</p>

            <p>Service Providers. We reserve the right to provide access to your Personal Information and Usage Data to our trusted service providers that assist us with the operation and maintenance of this Website and our communications network. For example, we may contract with third parties to process payments, host our servers, provide security, and provide production, fulfillment, optimization, analytics, and reporting services. Our service providers will be given access to your information only as is reasonably necessary to provide the services for which they are contracted.</p>

            <p>Successors. If we sell or otherwise transfer part or all of our business or assets to another organization, such as in the course of an acquisition, merger, bankruptcy or liquidation, we may transfer your Personal Information and Usage Data. In such an event, we will require the buyer or transferee to agree to our commitments provided in this Privacy Policy.</p>

            <p>Legal Process, Enforcement and Security Notice. We reserve the right to disclose your Personal Information and Usage Data if we have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary (i) to satisfy any applicable law, regulation, legal process or enforceable governmental request (such as for example, to comply with a subpoena or court order), (ii) to detect, prevent, and address fraud or other illegal activity, and (iii) to investigate, respond to, or enforce violations of our rights or the security of this Website.</p>

            <p>To Our Marketing Affiliates. We may participate with other companies or individuals for purposes of promoting our products, services, promotions or contests or their products, services, promotions, or contests. We reserve the right to disclose your Personal Information to them for purposes of (i) compensation, transaction processing, fulfillment, and support, and (ii) for purposes of offering you other products, services, promotions, and contests. These marketing affiliates may also contact you regarding other products, services, promotions, or contests.</p>

            <p>If you do not want us to share your Personal Information with any third parties, please email us by submitting our <a href="contact">contact form</a>.  Please note that your Personal Information will need to be shared with certain third-parties in order to process and fulfill any orders that you place over the Website.</p>

            <h2>CALIFORNIA PRIVACY RIGHTS</h2>
            <p>As described in this Privacy Policy, from time to time we may make your personal information available to third parties for their marketing purposes.  California law permits individuals who are California residents to request certain information about our disclosure of personal information to third parties for direct marketing purposes.  If you are a California resident and would like to make such a request, please submit your request in an e-mail via our <a href="contact">contact form</a>, with the phrase “California Privacy Request” in the subject line along with your name, address and email address. We will respond to you within thirty days of receiving such a request.  If you do not want us to share your personal information with third parties, you may opt-out of this information sharing by emailing us via our <a href="contact">contact form</a>.  In accordance with California Civil Code Sec. 1789.3, California resident users are entitled to know that they may file grievances and complaints with the California Department of Consumer Affairs, 1625 North Market Blvd., Suite N 112, Sacramento, CA 95834; or by phone at 916-445-1254 or 800-952-5210; or by email to dca@dca.ca.gov. </p>

            <h2>SPECIFIC INFORMATION ABOUT COOKIES AND WEB BEACONS</h2>
            <p>In order to provide better service for our Website, we may use Cookies and Web Beacons to collect Usage Data to store your preferences and information about what pages you visit and past activity at our Website. We may also employ Web Beacons from third parties in order to help us compile aggregated statistics regarding the effectiveness of our promotional campaigns or other operations of our Website.</p>

            <p>“Cookies” are tiny pieces of information stored by your browser on your computer’s hard drive. Cookies are also used to customize content based on your browser. Most browsers are initially set to accept cookies. If you want to disable cookies, there is a simple procedure in most browsers that allows you to turn off cookies. Please remember, however, that cookies may be required to allow you to use certain features of our Website.</p>

            <p>Flash Cookies - third party cookies that use an Adobe Flash Media Player local shared object (LSO) - may be used along with other third party cookies for purposes of crediting any purchase you may make on this Website to one of our marketing affiliates that may have referred you to us. These cookies will be used for purposes of crediting sales to the referring marketing affiliate. Flash cookies are not the same as “browser cookies.” The Adobe Flash Media Player is software that enables users to view content on their computers. Flash cookies are also accompanied by a browser cookie. If you delete the browser cookie, the Flash cookie may automatically create (or re-spawn) a replacement for the browser cookie.</p>

            <p>Web Beacons - sometimes called single-pixel gifs or clear gifs - are used to assist in delivering cookies, and they allow us to count users who have visited pages of our Website. We may include Web Beacons in promotional e-mail messages or other communications in order to determine whether messages have been opened and acted upon.</p>

            <h2>ANALYTICS</h2>

            <p>We reserve the right to participate with third party analytics partners to monitor and analyze Website traffic and track user behavior on this Website.</p>

            <p>Google Analytics (Google) - Google Analytics is a web analysis service provided by Google Inc. (“Google”). Google utilizes the data collected to track and examine the use of this Website, to prepare reports on its activities, and to share them with other Google services. Information collected: cookie and Usage Data. Visit Privacy Policy at <a href="https://www.google.com/intl/en/policies/?fg=1" target="_blank">https://www.google.com/intl/en/policies/?fg=1</a>. You may opt out of the Google Analytics service with the Google’s Browser Add-on that’s available at <a href="https://tools.google.com/dlpage/gaoptout/" target="_blank">https://tools.google.com/dlpage/gaoptout/</a>.</p>

            <p>We invite you to socialize and share your participation with this Website and purchases. If you choose to use social media platforms such as Facebook, Twitter, Pinterest, and Instagram, you will be allowing interaction with these platforms or other external platforms directly from this Website, and in the process you may be sharing certain profile elements, including your comments. This sharing is subject to each social media program’s privacy policies.</p>

            <h2>INTEREST-BASED ADVERTISING</h2>
            <p>We also may share Usage Data with advertisers and other third parties who may use it for advertising purposes, including to serve targeted advertising on non-affiliated third party sites.  Some of those ads may be personalized, meaning that they are intended to be relevant to you based on information about your online activities on the Website or other websites over time.  For example, anonymous information collected across multiple sites may enable the ad network to predict your preferences and show you ads that are likely to be of interest to you.  Please note that, except as explained above with respect to our Affiliated Entities and Marketing Affiliates, we do not share any information that identifies you personally with the third party service providers who serve ads on our behalf.  Please visit the Network Advertising Initiative (“NAI”) for information about how to opt-out of interest-based advertising by their members. See http://www.networkadvertising.org for general information about the NAI and http://www.networkadvertising.org/managing/opt_out.asp for the opt-out page.  You may also visit http://www.aboutads.info/consumers/ to learn about interest-based advertising and how to opt-out from online behavioral ads served by some or all participating companies.</p>

            <h2>E-MAIL MARKETING</h2>
            <p>By submitting your email address through the Website, you are expressly consenting to receive emails from The Company, including from The Company’s affiliates, and from third parties concerning offers and advertisements unrelated to The Company.  To opt-out of receiving email messages from us, from our affiliates or from other third parties, click on the “Unsubscribe” link contained in each email.  Please allow up to 10 business days for your request to be processed.  Please note that if you decide not to receive marketing emails from us, you may still receive transactional email messages regarding your order(s) (i.e., order confirmation, shipping information, etc.).  If you have questions or concerns regarding this provision, please <a href="contact">contact us</a>.   </p>

            <h2>DO NOT TRACK REQUESTS</h2>
            <p>Some Web browsers incorporate a “Do Not Track” feature that signals to websites that you visit that you do not want to have your online activity tracked. Each browser communicates “Do Not Track” signals to websites differently, making it unworkable to honor each and every request correctly. In order to alleviate any communication error between browsers and the Website, we do not respond to “Do Not Track” signals at this time. As the technology and communication between browser and website improves, we will reevaluate the ability to honor “Do Not Track” signals and may make changes to our policy.</p>

            <h2>DATA SECURITY</h2>
            <p>We will implement reasonable and appropriate security procedures consistent with prevailing industry standards to protect data from unauthorized access by physical and electronic intrusion. Unfortunately, no data transmission over the Internet or method of data storage can be guaranteed 100% secure. Therefore, while we strive to protect your Personal Information by following generally accepted industry standards, we cannot ensure or warrant the absolute security of any information you transmit to us or archive at this Website.</p>

            <h2>ONWARD TRANSFER OUTSIDE YOUR COUNTRY OF RESIDENCE</h2>
            <p>Any Personal Information which we may collect on this Website may be stored and processed in our servers located in the United States or in any other country in which we, or our affiliates, subsidiaries, or agents maintain facilities. By using this Website, you consent to any such transfer of Personal Information outside your country of residence to any such location.</p>

            <h2>UPDATING PERSONAL INFORMATION</h2>
            <p>At any time you may contact The Company via <a href="contact">email</a> to update your Personal Information or request that your Personal Information no longer be shared by The Company.  In the case that your credit or other payment card is lost, stolen, or used without permission, promptly contact The Company using our <a href="contact">contact form</a> to update the Personal Information we have on file for you.</p>

            <p>We may request identification prior to approving any requests to update your Personal Information.  This is designed to protect your privacy.  We reserve the right to decline any requests that are unreasonably repetitive or systematic, require unreasonable time or effort of our technical or administrative personnel, or undermine the privacy rights of others. We reserve the right to permit you to access your Personal Information in any account you establish with this Website for purposes of making your own changes or updates, and in such case, instructions for making such changes or updates will be provided where necessary.</p>

            <h2>LINKS TO THIRD-PARTY SITES</h2>
            <p>This Website may contain links to other websites operated by third-parties. If you do click on any of the links to their websites or accept any of their promotional offers, your click-through information and any information that you provide in the process of registration or purchase will be transferred to these sites. We have no responsibility or liability for the policies and practices of these third-party sites. You should be careful to review any privacy policies posted on any of these sites before providing information to them.</p>

            <h2>CHILDREN'S ONLINE POLICY</h2>
            <p>We are committed to preserving online privacy for all Website visitors, including children. This Website is targeted at adults only. Consistent with the Children’s Online Privacy Protection Act (COPPA), we will not knowingly collect any information from, or sell to, children under the age of 13. If you are a parent or guardian who has discovered that your child under the age of 13 has submitted his or her personally identifiable information without your permission or consent, we will remove the information from our active list, at your request. To request the removal of your child’s information, please contact our site as provided below under “Contact Us,” and be sure to include in your message the same login information that your child submitted.</p>

            <h2>CONTACT US</h2>
            <p>If you have any questions regarding this Privacy Policy, please contact the owner and operator of this website business at:</p>
            <p>Attn: Privacy Policy</p>
            <img width="223" src="//d2e2sm5hsxwj7z.cloudfront.net/aki1/dist/images/address.png">
            <br><br>
            <p>Email: <a href="contact">Contact Form</a><br>
                Telephone: 866-978-7310                                    </div>
    </section>
</main>
</body>
</html>
