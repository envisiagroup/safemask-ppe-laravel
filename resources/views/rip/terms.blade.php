<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PCTCKQ3');</script>
<!-- End Google Tag Manager -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Terms | SafeMaskPPE</title>
    <meta name="robots" content="noindex, nofollow">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="//d2e2sm5hsxwj7z.cloudfront.net/aki1/dist/css/popup.min.css">
    <link href="//www.safemaskppe.com/favicon.ico" rel="icon" type="image/x-icon">
</head>


<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCTCKQ3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->	<header>
    <h1 align="center"><strong>Terms and Conditions</strong></h1>
</header>
<main>
    <section>
        <div class="container">
            <br>
            <p>By placing an order through this website, you agree to the terms and conditions set forth
                below. Please read through these terms and conditions carefully before placing your
                order, and print a copy for future reference. Please also read our <a
                    href="privacy">Privacy Policy</a>
                regarding personal information provided by you, which is incorporated herein by
                reference.</p>

            <h2>Health Disclaimer</h2>
            <p>The statements made on this website, as well as any materials or supplements distributed
                or sold on SafeMaskPPE.com, have not been evaluated by the Food and Drug Administration.
                Products featured on SafeMaskPPE.com are not intended to diagnose, treat, cure, or prevent
                any disease. If you are pregnant, nursing, taking medication, or have a history of heart
                conditions, we suggest consulting with a healthcare professional before using any of our
                products. The results achieved with all products are not typical, and not everyone will
                experience these results.</p>

            <h2>The Guarantee:</h2>
            <p><b>Return Policy for Standard Deliveries (including Single Box, Bundle Shipment, and
                    additional offers)</b></p>
            <p>SafeMaskPPE.com offers a 30 day satisfaction guarantee on all standard shipments. If you,
                the buyer, are unhappy with the product for any reason — even if you've used a full 30
                day supply of the supplement — you can return the empty boxes, plus any additional
                unopened boxes, for a full refund of the purchase price, minus shipping and processing
                (if applicable). Please note that 30 days worth of product can be empty, but the
                remaining units must be completely sealed, unused and returned.</p>
            <p>To obtain your refund call us at 866-978-7310 or <a
                    href="contact">email us</a>. You will be given a Return Merchandise
                Authorization (RMA) number. To receive your refund, your return must be received at our
                shipping facility within 30 days of purchase. Be sure to clearly write the return
                merchandise authorization (RMA) number on the outside of the box. Our shipping
                department is NOT allowed to accept any packages without an RMA number. You will pay for
                return shipping.</p>
            <p>We will not accept or issue a refund for any refused packages or packages marked “return
                to sender” or refused that do not include a valid RMA number. If you return a package,
                we recommend that you obtain proof of shipment. Upon receipt of your returned product
                with a valid RMA number, a refund will be issued to your credit card. After the shipping
                department receives your return, it generally takes 3-5 business days or less to process
                your refund. Once a return is processed, it can take up to one billing cycle for this
                return to be posted to your account, depending on your credit card company or financial
                institution.</p>
            <p>Note that SafeMaskPPE.com and its parent company reserves the right to deny/cancel any
                future shipments or returns to and from any individual or shipping address found to be
                abusing our policies or breaching our terms.</p>

            <h2>TERMS OF SERVICE</h2>
            <p>This Terms of Service (“TOS”) is a legally binding agreement made by and between Vital
                Research Group (“we” or “us”) and you, personally and, if applicable, on behalf of the
                entity for whom you are using this website (collectively, “you”). This TOS governs your
                use of the SafeMaskPPE.com website (“Website”) and the services we offer on the Website
                (“Services”), so please read it carefully.</p>
            <p><b>BY ACCESSING OR USING ANY PART OF THE WEBSITE, YOU AGREE THAT YOU HAVE READ,
                    UNDERSTAND, AND AGREE TO BE BOUND BY THIS TOS. IF YOU DO NOT AGREE TO BE SO BOUND,
                    DO NOT ACCESS OR USE THE WEBSITE.</b></p>
            <p><b>INTERNET TECHNOLOGY AND THE APPLICABLE LAWS, RULES, AND REGULATIONS CHANGE FREQUENTLY.
                    ACCORDINGLY, WE RESERVE THE RIGHT TO MAKE CHANGES TO THIS TOS AT ANY TIME. YOUR
                    CONTINUED USE OF THE WEBSITE CONSTITUTES ASSENT TO ANY NEW OR MODIFIED PROVISION OF
                    THIS TOS THAT MAY BE POSTED ON THE WEBSITE.</b></p>
            <p><b>THIS AGREEMENT CONTAINS ARBITRATION AND CLASS ACTION WAIVER PROVISIONS THAT WAIVE YOUR
                    RIGHT TO A COURT HEARING OR JURY TRIAL OR TO PARTICIPATE IN A CLASS ACTION.
                    ARBITRATION IS MANDATORY AND IS THE EXCLUSIVE REMEDY FOR ANY AND ALL DISPUTES.</b>
            </p>

            <h3>1. Using the Website.</h3>
            <p>(a) Eligibility. Except as expressly provided below, Services may only be used by, and
                Membership is limited to, individuals who can form legally binding contracts under
                applicable law. Without limitation, minors are prohibited from becoming Members and,
                except as specifically provided below, using fee-based Services. Membership is defined
                by engaging in a purchase agreement with Vital Research Group wherein you, the consumer,
                purchase one of the products found on the Website.</p>
            <p>(b) Compliance. You must comply with all of the terms and conditions of this TOS, the
                policies referred to below, and all applicable laws, regulations, and rules when you use
                the Website.</p>
            <p>(c) License and Restrictions. Subject to the terms and conditions of this TOS, you are
                hereby granted a limited, non-exclusive right to use the content and materials on the
                Website. You may not use any third-party intellectual property without the express
                written permission of the applicable third party, except as permitted by law. The
                Website will retain ownership of its intellectual property rights, and you may not
                obtain any rights therein by virtue of this TOS or otherwise, except as expressly set
                forth in this TOS. You will have no right to use, copy, display, perform, create
                derivative works from, distribute, have distributed, transmit, or sublicense from
                materials or content available on the Website, except as expressly set forth in this
                TOS. You may not attempt to reverse engineer any of the technology used to provide the
                Services.</p>
            <p>(d) Prohibited Conduct. In your use of the Website and the Services, you may not: (i)
                infringe any patent, trademark, trade secret, copyright, right of publicity, or other
                right of any party; (ii) defame, abuse, harass, stalk any individual, or disrupt or
                interfere with the security or use of the Services, the Website, or any websites linked
                to the Website; (iii) interfere with or damage the Website or Services, including,
                without limitation, through the use of viruses, cancel bots, Trojan horses, harmful
                code, flood pings, denial-of-service attacks, packet or IP spoofing, forged routing of
                electronic mail address information, or similar methods or technology; (iv) attempt to
                use another user's account, impersonate another person or entity, misrepresent your
                affiliation with a person or entity, including, without limitation, the Website, or
                create or use a false identity; (v) attempt to obtain unauthorized access to the Website
                or portions of the Website that are restricted from general access; (vi) engage,
                directly or indirectly, in transmission of "spam," chain letters, junk mail, or any
                other type of unsolicited solicitation; (vii) collect, manually or through an automatic
                process, information about other users without their express consent or other
                information relating to the Website or the Services; (viii) use any meta tags or any
                other “hidden text” utilizing the product names or trademarks, of products listed on
                this website; (ix) advertise, offer to sell, or sell any goods or services, except as
                expressly permitted by the Website; (x) engage in any activity that interferes with any
                third party's ability to use or enjoy the Website or Services; or (xi) assist any third
                party in engaging in any activity prohibited by this TOS.</p>
            <p>(e) Other Users. If you become aware of any conduct that violates this TOS, we encourage
                you to contact Customer Service. We reserve the right, but will have no obligation, to
                respond to such communications.</p>

            <h3>2. Your Content.</h3>
            <p>(a) License. By posting, storing, or transmitting any content on or to the Website, you
                hereby grant us a perpetual, worldwide, non-exclusive, royalty-free, sub-licensable
                right and license to use, copy, display, perform, create derivative works from,
                distribute, have distributed, transmit, and sublicense such content in any form, in all
                media now known or hereinafter created, anywhere in the world. You hereby irrevocably
                waive any claims based on moral rights or similar theories, if any.</p>
            <p>(b) Objectionable Content. We do not have the ability to control the nature of the
                user-generated content offered through the Website. You are solely responsible for your
                interactions with other users of the Website and any content that you post. We will not
                be liable for any damage or harm resulting from any content, nor from your interactions
                with other users of the Website. We reserve the right, but have no obligation, to
                monitor interactions between you and other users of the Website and take any other
                action to restrict access to or the availability of any material that we or another user
                of the Website may consider to be obscene, lewd, lascivious, filthy, excessively
                violent, harassing, or otherwise objectionable (including, without limitation, because
                it violates this TOS).</p>

            <h3>3. Accuracy of Information.</h3>
            <p>We attempt to ensure that the information on the Website is complete and accurate;
                however, this information may contain typographical errors, pricing errors, and other
                errors or inaccuracies. We assume no responsibility for such errors and omissions, and
                reserve the right to: (i) revoke any offer stated on the Website at any time; (ii)
                correct any errors, inaccuracies, or omissions; and (iii) make changes to prices,
                content, promotions, product descriptions or specifications, or other information on the
                Website at any time.</p>

            <h3>4. Sales Tax.</h3>
            <p>If you purchase any products available on the Website (“Products”), you will be
                responsible for paying any applicable sales tax indicated on the Website.</p>

            <h3>5. Fraud.</h3>
            <p>We reserve the right, but undertake no obligation, to actively report and prosecute
                actual and suspected credit card fraud. We may, in our discretion, require further
                authorization, such as a telephone confirmation of your order and other information,
                from you. We reserve the right to cancel, delay, refuse to ship, or recall from the
                shipper any order if fraud is suspected. We capture certain information during the order
                process, including time, date, IP address, and other information that will be used to
                locate and identify individuals committing fraud. If any Website order is suspected to
                be fraudulent, we reserve the right, but undertake no obligation, to submit all records,
                with or without a subpoena, to all law enforcement agencies, the credit card company,
                and any relevant financial institutions for fraud investigation. We reserve the right to
                cooperate with authorities to prosecute offenders to the fullest extent of the law.</p>

            <h3>6. Intellectual Property Rights.</h3>
            <p>(a) Copyright. All materials on the Website, including, without limitation, the logos,
                design, text, graphics, other files, and the selection and arrangement thereof are
                either owned by us or are the property of our suppliers, licensors, or other companies.
                You may not use such materials without express written permission.</p>
            <p>(b) Trademarks. All brand names listed on this site are trademarked or covered under
                first use common law trademark. We own the related design marks, and other trademarks on
                the Website. Page headers, custom graphics, button icons, and scripts are trademarks or
                trade dress we own. You may not use any of these trademarks, trade dress, or trade names
                without our express written permission.</p>

            <h3>7. Third-Party Websites.</h3>
            <p>SafeMaskPPE.com may contain links to other websites on the Internet that are owned and
                operated by third parties. We do not control the information, products, or services
                available on these third party websites. The inclusion of any link does not imply our
                endorsement of the applicable website or any association with the website's operators.
                Because we have no control over such websites and resources, you agree that we are not
                responsible or liable for the availability or the operation of such external websites,
                for any material located on or available from any such websites, or for the protection
                of your data privacy by third parties. Any dealings with, or participation in promotions
                offered by, advertisers on the Website, including the payment and delivery of related
                goods or services, and any other terms, conditions, warranties, or representations
                associated with such dealings or promotions, are solely between you and the applicable
                advertiser or other third party. You further agree that we shall not be responsible or
                liable, directly or indirectly, for any loss or damage caused by the use of or reliance
                on any such material available on or through any such website, or any such dealings or
                promotions. Notwithstanding anything in this TOS to the contrary, this section does not
                apply in the state of New Jersey.</p>

            <h3>8. Linking and Framing.</h3>
            <p>You may not deep link to portions of the Website, or frame, inline link, or similarly
                display any of our property, including, without limitation, the Website. You may not use
                any of our logos or other trademarks as part of a link without express written
                permission.</p>

            <h3>9. Comments.</h3>
            <p>All comments, feedback, suggestions, ideas, and other submissions that you disclose,
                submit, or offer to us in connection with your use of the Website will become our
                exclusive property. Such disclosure, submission, or offer of any comments shall
                constitute an assignment to us of all worldwide right, title, and interest in all
                patent, copyright, trademark, and all other intellectual property and other rights
                whatsoever in and to the comments, and a waiver of any claim based on moral rights,
                unfair competition, breach of implied contract, breach of confidentiality, and any other
                legal theory. You will, at our cost, execute any documents to effect, record, or perfect
                such assignment. Thus, we will own exclusively all such right, title, and interest, and
                shall not be limited in any way in the use, commercial or otherwise, of any comments.
                You should not submit any comments to us if you do not wish to assign such rights to us.
                We are and will be under no obligation to: (i) maintain any comments in confidence; (ii)
                pay to you or any third party any compensation for any comments; or (iii) respond to any
                comments. You are and shall remain solely responsible for the content of any comments
                you make.</p>

            <h3>10. Indemnification.</h3>
            <p>You agree to defend, indemnify, and hold the Website and its subsidiaries, affiliates,
                and their directors, officers, agents, members, shareholders, co-branders, or other
                partners, employees, and advertising partners harmless from any liabilities, losses,
                actions, damages, claims, or demands, including reasonable attorneys' fees, costs, and
                expenses, made by any third party directly or indirectly relating to or arising out of
                (a) content you provide to the Website or otherwise transmit or obtain through the
                Service, (b) your use of the Service, (c) your connection to the Service, (d) your
                violation of this Agreement, (e) your violation of any rights of another, or (f) your
                failure to perform your obligations hereunder. If you are obligated to provide
                indemnification pursuant to this provision, we may, in our sole and absolute discretion,
                control the disposition of any claim at your sole cost and expense. Without limitation
                of the foregoing, you may not settle, compromise, or in any other manner dispose of any
                claim without our consent. NOTWITHSTANDING ANYTHING IN THIS TOS TO THE CONTRARY, THIS
                SECTION DOES NOT APPLY IN NEW JERSEY.</p>

            <h3>11. DISCLAIMERS, EXCLUSIONS AND LIMITATIONS.</h3>
            <p>(a) DISCLAIMER OF WARRANTIES. WE PROVIDE THE WEBSITE, THE PRODUCTS, AND SERVICES ON AN
                “AS IS” AND “AS AVAILABLE” BASIS. WE DO NOT REPRESENT OR WARRANT THAT THE PRODUCTS, THE
                WEBSITE, THE SERVICES, THEIR USE, AND ANY INFORMATION ON CONTAINED THEREIN: (I) WILL BE
                UNINTERRUPTED OR SECURE, (II) WILL BE FREE OF DEFECTS, INACCURACIES, OR ERRORS, (III)
                WILL MEET YOUR REQUIREMENTS, OR (IV) WILL OPERATE IN THE CONFIGURATION OF OR WITH OTHER
                HARDWARE OR SOFTWARE YOU USE. WE MAKE NO WARRANTIES OTHER THAN THOSE MADE EXPRESSLY IN
                THIS TOS, AND HEREBY DISCLAIM ANY AND ALL IMPLIED WARRANTIES, INCLUDING, WITHOUT
                LIMITATION, WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR CAUSE, MERCHANTABILITY,
                AND NON-INFRINGEMENT.</p>
            <p>(b) DISCLAIMER OF FORWARD-LOOKING STATEMENTS. THIS WEBSITE MAY CONTAIN FORWARD-LOOKING
                STATEMENTS THAT REFLECT OUR CURRENT EXPECTATION REGARDING FUTURE EVENTS AND BUSINESS
                DEVELOPMENT. THE FORWARD-LOOKING STATEMENTS INVOLVE RISKS AND UNCERTAINTIES. ACTUAL
                DEVELOPMENTS OR RESULTS COULD DIFFER MATERIALLY FROM THOSE PROJECTED AND DEPEND ON A
                NUMBER OF FACTORS, SOME OF WHICH ARE OUTSIDE OUR CONTROL.</p>
            <p>(c) HEALTH-RELATED INFORMATION. WE PROVIDE INFORMATION ON THE WEBSITE FOR INFORMATIONAL
                PURPOSES ONLY. IT IS NOT MEANT AS A SUBSTITUTE FOR THE ADVICE OF A DOCTOR OR OTHER
                HEALTHCARE PROFESSIONAL. YOU SHOULD NOT USE THE INFORMATION AVAILABLE ON OR THROUGH THE
                WEBSITE FOR DIAGNOSING OR TREATING A MEDICAL CONDITION. YOU SHOULD CAREFULLY READ ALL
                PRODUCT INSTRUCTIONS PRIOR TO USE.</p>
            <p>(d) PRODUCTS. ALL PRODUCTS ARE SUBJECT ONLY TO ANY APPLICABLE WARRANTIES OF THEIR
                RESPECTIVE MANUFACTURERS, DISTRIBUTORS, AND SUPPLIERS, IF ANY, PROVIDED IN THE PRODUCT
                PACKAGING. TO THE FULLEST EXTENT PERMISSIBLE BY APPLICABLE LAW, WE HEREBY DISCLAIM ALL
                WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY
                IMPLIED WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR
                PURPOSE OR CAUSE. WITHOUT LIMITING THE GENERALITY OF THE FOREGOING, WE HEREBY EXPRESSLY
                DISCLAIM ALL LIABILITY FOR PRODUCT DEFECT OR FAILURE, CLAIMS THAT ARE DUE TO NORMAL
                WEAR, PRODUCT MISUSE, ABUSE, PRODUCT MODIFICATION, IMPROPER PRODUCT SELECTION,
                NONCOMPLIANCE WITH ANY CODES, OR MISAPPROPRIATION.</p>
            <p>(e) EXCLUSION OF DAMAGES. WE WILL NOT BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY
                CONSEQUENTIAL, INCIDENTAL, INDIRECT, PUNITIVE, OR SPECIAL DAMAGES (INCLUDING, WITHOUT
                LIMITATION, DAMAGES RELATING TO LOST PROFITS, LOST DATA, OR LOSS OF GOODWILL) ARISING
                OUT OF, RELATING TO, OR CONNECTED WITH THE USE OF THE WEBSITE OR PRODUCTS, REGARDLESS OF
                THE CAUSE OF ACTION ON WHICH THEY ARE BASED, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
                DAMAGES OCCURRING.</p>
            <p>(f) LIMITATION OF LIABILITY. IN NO EVENT WILL OUR AGGREGATE LIABILITY ARISING FROM,
                RELATING TO, OR IN CONNECTION WITH THIS TOS, INCLUDING, WITHOUT LIMITATION, CLAIMS
                RELATING TO THE WEBSITE OR THE PRODUCTS, EXCEED THE GREATER OF $100 OR THE AMOUNT THAT
                YOU PAID FOR THE PRODUCTS. NOTWITHSTANDING ANYTHING IN THIS TOS TO THE CONTRARY, THIS
                DISCLAIMER OF WARRANTY, EXCLUSIONS, AND LIMITATIONS DO NOT APPLY IN NEW JERSEY.</p>

            <h3>12. Force Majeure.</h3>
            <p>You acknowledge and understand that if the Website is unable to provide the Products as a
                result of a force majeure event, the Website will not be in breach of any of its
                obligations toward you under these TOS. A force majeure event means any event beyond the
                control of the Website. THE WEBSITE SHALL NOT HAVE ANY LIABILITY TO YOU, WHETHER IN
                CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE), OR ANY OTHER FORM OF LIABILITY FOR
                FAILING TO PERFORM ITS OBLIGATIONS UNDER THIS AGREEMENT TO THE EXTENT THAT SUCH FAILURE
                IS AS A RESULT OF A FORCE MAJEURE EVENT. THIS SECTION SHALL NOT APPLY IN THE STATE OF
                NEW JERSEY.</p>

            <h3>13. Domestic Use and Export Restriction.</h3>
            <p>We control the Website from our offices within the United States of America. We make no
                representation that the Website or its content (including, without limitation, any
                products or services available on or through the Website) are appropriate or available
                for use in other locations. Users who access the Website from outside the United States
                of America do so on their own initiative and must bear all responsibility for compliance
                with local laws, if applicable. Further, the United States export control laws prohibit
                the export of certain technical data and software to certain territories. No content
                from the Website may be downloaded in violation of United States law.</p>

            <h3>14. Arbitration and Waiver of Class Action Rights.</h3>
            <p>This TOS and any issue or dispute arising out of or otherwise related to this TOS or your
                use of our Website, the <a href="privacy">Privacy Policy</a>, or any matter
                concerning Force Factor including use and purchase of any product and enrollment in the
                Membership Program, collectively "Disputes," shall be governed exclusively by the laws
                of Florida, excluding its conflict of law provisions. If a Dispute arises, we agree to
                first contact each other with a written description of the Dispute, all relevant
                documents and information, and the proposed resolution. You agree to contact us with
                Disputes by writing to us at the address specified in section 4.3 of our <a
                    href="privacy">Privacy Policy</a>.</p>
            <p>If any Dispute cannot be resolved informally, we each agree that any and all Disputes,
                other than those filed in small claims court, shall be submitted to final and binding
                arbitration before a single arbitrator of the American Arbitration Association ("AAA")
                in a location convenient to you. Either party may commence the arbitration process by
                submitting a written demand for arbitration with the AAA, and providing a copy to the
                other party. The arbitration will be conducted in accordance with the provisions of the
                AAA's Commercial Dispute Resolutions Procedures, Supplementary Procedures for
                Consumer-Related Disputes, in effect at the time of submission of the demand for
                arbitration. We will pay all of the filing costs, including arbitrator fees. Judgment on
                the award rendered by the arbitrator may be entered in any court of competent
                jurisdiction. Notwithstanding the foregoing, the following shall not be subject to
                arbitration and may be adjudicated only in the state and federal courts of Florida: (i)
                any dispute, controversy, or claim relating to or contesting the validity of the our
                proprietary rights, including without limitation, trademarks, service marks, copyrights,
                or trade secrets; or (ii) an action by a party for temporary, preliminary, or permanent
                injunctive relief, whether prohibitive or mandatory, or other provisional relief. <b>You
                    expressly agree to refrain from bringing or joining any claims in any representative
                    or class-wide capacity, including but not limited to bringing or joining any claims
                    in any class action or any class-wide arbitration.</b></p>
            <p><b>YOU UNDERSTAND THAT YOU WOULD HAVE HAD A RIGHT TO LITIGATE THROUGH A COURT, TO HAVE A
                    JUDGE OR JURY DECIDE YOUR CASE, AND TO BE PARTY TO A CLASS OR REPRESENTATIVE ACTION.
                    HOWEVER, YOU UNDERSTAND AND CHOOSE TO HAVE ANY CLAIMS DECIDED INDIVIDUALLY AND ONLY
                    THROUGH ARBITRATION.</b></p>

            <h3>15. Modification of Terms of Service.</h3>
            <p>We reserve the right to change or modify these TOS at any time, with or without notice,
                and your continued use of the Website will be conditioned upon the TOS in force at the
                time of your use. You can always check the most current version of the TOS at this page.
            </p>

            <h3>16. Termination.</h3>
            <p>We maintain the right to terminate your access to the Website if we reasonably believe
                you have breached any of the TOS. Following termination, you will not be permitted to
                use the Website and we may, in our sole discretion, cancel any outstanding Product
                orders. If your access to the Website is terminated, we reserve the right to exercise
                whatever means we deem necessary to prevent unauthorized access to the Website,
                including, but not limited to, technological barriers, IP mapping, and direct contact
                with your Internet Service Provider. This TOS will survive indefinitely unless and until
                we choose to terminate it, with or without notice, regardless of whether any account or
                order you open is terminated by you or us, or if you have the right to access or use the
                Website.</p>

            <h3>17. Integration.</h3>
            <p>This TOS contains the entire understanding between you and us regarding the use of the
                Website, and supersedes all prior and contemporaneous agreements and understandings
                between you and us relating thereto.</p>

            <h3>18. Additional Terms.</h3>
            <p>These TOS, and all of your rights and obligations under them, may not be assigned or
                transferred by you without our prior written consent. No failure or delay by a party in
                exercising any right, power, or privilege under this TOS will operate as a waiver
                thereof, nor will any single or partial exercise of any right, power, or privilege
                preclude any other or further exercise thereof, or the exercise of any other right,
                power, or privilege under this TOS. You are an independent contractor, and no agency,
                partnership, joint venture, or employee-employer relationship is intended or created by
                this TOS. The invalidity or unenforceability of any provision of this TOS will not
                affect the validity or enforceability of any other provision of this TOS, all of which
                will remain in full force and effect.</p>

            <h3>19. Use of personal information</h3>
            <p>By completing any forms on our Website you grant the Company the right to use the
                collected information for marketing purposes including, but not limited to, sharing such
                information with third party advertisers ("Advertisers"), emailing, SMS Message, or
                physically mailing Company or any third party offers to your email address or postal
                address. We may also use such information to track compliance with the applicable order,
                or for content improvement and feedback purposes. We may share the personal information
                that you supply to us and we may join together with other businesses to bring selected
                retail or service opportunities to our user base. These businesses may include providers
                of direct marketing services and applications, including lookup and reference, data
                enhancement, suppression and validation. Company will not share, trade, or sell credit
                card information or Personal Information to any 3rd party. In addition, the Company
                reserves the right to release current or past user information in the event we believe
                that the Website is being or has been used in violation of any rules; to commit unlawful
                acts; if the information is subpoenaed; if the Company is sold or acquired; or when the
                Company deems it necessary or appropriate. By agreeing to these terms, you hereby
                consent to disclosure of any record or communication to any third party when the
                Company, in its sole discretion, determines the disclosure to be appropriate. We may
                share Website usage information about our Website visitors who have received targeted
                promotional campaigns with Advertisers for the purpose of formatting future campaigns
                and upgrading visitor information used in reporting statistics. The Company also
                reserves the right to provide aggregate or group data about our visitors and users for
                lawful purposes. Aggregate or group data is data that describes the demographics, usage,
                or characteristics of our participants as a group, without revealing any personally
                identifiable information. By subscribing to the Website, you agree to allow us to
                provide such data to third parties.</p>

            <h3>20. Wireless Policy to add to existing Terms of Use/Privacy Policy</h3>
            <p>We may use personal information to provide the services you've requested, including
                services that display customized content and advertising. In addition to any fee of
                which you are notified, your provider's standard messaging rates apply to our
                confirmation and all subsequent SMS correspondence. You may opt-out and remove your SMS
                information by sending "STOP", "END", "QUIT" to the SMS text message you have received.
                If you remove your SMS information from our database it will no longer be used by us for
                secondary purposes, disclosed to third parties, or used by us or third parties to send
                promotional correspondence to you.</p>

            <h3>21. Detailed Wireless Policy</h3>
            <p>Data obtained from you in connection with this SMS service may include your name,
                address, cell phone number, your provider's name, and the date, time, and content of
                your messages. In addition to any fee of which you are notified, your provider's
                standard messaging rates apply to our confirmation and all subsequent SMS
                correspondence. All charges are billed by and payable to your mobile service provider.
                We will not be liable for any delays in the receipt of any SMS messages, as delivery is
                subject to effective transmission from your network operator. SMS message services are
                provided on an AS IS basis.</p>
            <p>We may use personal information to provide the services you've requested, including
                services that display customized content and advertising. We may also use personal
                information for auditing, research and analysis to operate and improve our technologies
                and services. We may share aggregated and non personal information with third parties
                outside of Auction Monster. When we use third parties to assist us in processing your
                personal information, we require that they comply with our Privacy Policy and any other
                appropriate confidentiality and security measures. We may also share information with
                third parties in limited circumstances, including when complying with legal process,
                preventing fraud or imminent harm, and ensuring the security of our network and
                services. You may remove your information from our database. If you remove your
                information from our database it will no longer be used by us for secondary purposes,
                disclosed to third parties, or used by us or third parties to send promotional
                correspondence to you. You may remove your information by sending your request in
                writing via <a href="contact">email</a> or by sending "STOP", "END", "QUIT" to the
                SMS text message you have received.</p>
        </div>
    </section>
</main>
<footer>
    <div class="container">
        <p>&copy; 2020 SafeMaskPPE.com, All Rights Reserved.</p>
    </div>
</footer>
</body>
</html>
