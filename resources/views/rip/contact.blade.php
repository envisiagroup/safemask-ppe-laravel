
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PCTCKQ3');</script>
<!-- End Google Tag Manager -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Contact | SafeMaskPPE</title>
    <link href="//d2e2sm5hsxwj7z.cloudfront.net/aki1/dist/css/popup.min.css" rel="stylesheet" type="text/css" />
    <link href="//www.safemaskppe.com/favicon.ico" rel="icon" type="image/x-icon">
</head>
<body id="contact">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PCTCKQ3"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->    <header>
    <h1 align="center"><strong>Contact Us</strong></h1>
</header>
<main>
    <section>
        <div class="container">
            <form action="/include/shared_libraries/response_forms/mmex.php" method="post" enctype="multipart/form-data" id="contact_form" name="contact_form">
                <input type="hidden" name="Settings" value="/include/shared_libraries/response_forms/settings.php" />
                <div class="form-group">
                    <div class="left">
                        <label>Subject:<span class="red">*</span></label>
                        <div class="clear"></div>
                    </div>
                    <div class="right">
                        <input name="subject" id="subject" type="text" class="form-control" required/>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group">
                    <div class="left">
                        <label>Comments:</label>
                        <div class="clear"></div>
                    </div>
                    <div class="right">
                        <textarea name="comments" cols="25" rows="5" class="form-control" required ></textarea>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group">
                    <div class="left">
                        <label>Your Name:<span class="red">*</span></label>
                        <div class="clear"></div>
                    </div>
                    <div class="right">
                        <input name="name" type="text" class="form-control" required />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group">
                    <div class="left">
                        <label>Email:<span class="red">*</span></label>
                        <div class="clear"></div>
                    </div>
                    <div class="right">
                        <input name="email" type="email" class="form-control" required />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group">
                    <div class="left">
                        <label>Telephone #:</label>
                        <div class="clear"></div>
                    </div>
                    <div class="right">
                        <input name="phone" maxlength="20" type="tel" class="form-control" required />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Submit" onclick="allow_exit = true;" />
                    <input type="reset" value="Reset" />
                </div>
                <div class="form-group">
                    <p class="red">*Required</p>
                    <button style="display:none;" id="contact_captcha" class="g-recaptcha invisible-recaptcha" />Submit</button>
                </div>
            </form>

        </div>
    </section>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="../include/shared_libraries/jquery.validate.min.js"></script>
<script src="../include/shared_libraries/validations.js"></script>
<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>
<script>var recaptcha_sitekey = '6LclJa4UAAAAAMf4f69Yse1-qNnRvZETJnGvyTiM';</script>
</body>
</html>
