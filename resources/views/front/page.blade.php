@extends('front.layouts.app')

@section('content')
<!-- HERO BANNER -->
<section class="hero-banner small background no-padding">
    <div class="floral-pattern" data-stellar-background-ratio="0.8"></div>
    <span class="triangle triangle--top-left-small" style="border-width: 50px 0px 0px 1388px;"></span>
</section>
<!-- END HERO BANNER -->

<!-- CONTENT -->
<section class="terms-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h3 class="moist-heading text-center no-padding-top no-after"><span>Last updated on {{ $page->updated_at->timezone('America/Denver')->format('F j, Y') }}</span></h3>
                    <h1 class="text-center page-title">{{ $page->title }}</h1>
                </div>
            </div>
        </div><!-- END Row -->
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
        		{!! $page->body !!}
        	</div>
        </div>
    </div>
</section>
@endsection