<!-- header top -->
            <div class="header-top">
                <div class="container">
                    <div class="header-inner">
                        <div class="header-left">
                            <div class="header-message">
                                <p>
                                    @if(\Cart::subtotal() == 0)
                                        Free Shipping on Orders over $100!
                                    @elseif(\Cart::subtotal() > 0 && \Cart::subtotal() < 100)
                                        You're only ${{ number_format(100 - \Cart::subtotal(), 2, '.', '0') }} away from Free Shipping!
                                    @elseif(\Cart::subtotal() >= 100)
                                        You've qualified for Free Shipping!
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="header-right">
                            <div class="header-control">
                                <div class="header-user envy-dropdown">
                                    <a href="javascript:void(0)" data-envy="envy-dropdown" class="link-dropdown"><span class="icon fa fa-user"></span><span class="text">Login/Register</span></a>
                                    <div class="sub-menu">
                                        <div class="user-head">
                                            <h3 class="user-title">Login</h3>
                                            <a href="javascript:void(0)" data-page="register" data-register="Create an Account" data-login="Login an Account" class="user-toggle">Create an account</a>
                                        </div>
                                        <div class="user-content">
                                            <div class="login-form">
                                                <form id="loginform" class="user-form loginform" method="post">
                                                    <p class="login-name">
                                                        <label for="login-name">
                                                            <input type="text" name="name" id="login-name" class="input" placeholder="User/Email *">
                                                        </label>
                                                    </p>
                                                    <p class="login-pass">
                                                        <label for="login-pass">
                                                            <input type="password" name="pass" id="login-pass" class="input" placeholder="Password *">
                                                        </label>
                                                    </p>
                                                    <p class="login-submit">
                                                        <label for="login-submit">
                                                            <input type="submit" name="submit" id="login-submit" class="button" value="LOGIN">
                                                        </label>
                                                    </p>
                                                    <p class="login-rememberme">
                                                        <label for="login-rememberme">
                                                            <input type="checkbox" name="rememberme" id="login-rememberme" class="remember">
                                                            <span>Save me</span>
                                                        </label>
                                                    </p>
                                                    <a href="#" class="forget-pass">Forget ur password?</a>
                                                </form>
                                                <div class="login-other">
                                                    <h4 class="title">You can Login with</h4>
                                                    <a href="#" class="button facebook"><span class="fa fa-facebook"></span>FACEBOOK</a>
                                                    <a href="#" class="button google"><span class="fa fa-google-plus"></span>GOOGLE+</a>
                                                </div>
                                            </div>
                                            <div class="register-form">
                                                <form id="registerform" class="user-form registerform" method="post">
                                                    <p class="register-name">
                                                        <label for="register-name">
                                                            <input type="text" name="name" id="register-name" class="input" placeholder="User/Email *">
                                                        </label>
                                                    </p>
                                                    <p class="register-pass">
                                                        <label for="register-pass">
                                                            <input type="password" name="pass" id="register-pass" class="input" placeholder="Password *">
                                                        </label>
                                                    </p>
                                                    <p class="register-submit">
                                                        <label for="register-submit">
                                                            <input type="submit" name="submit" id="register-submit" class="button" value="REGISTER">
                                                        </label>
                                                    </p>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>