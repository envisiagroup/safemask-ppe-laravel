<!-- header sticky -->
        <div class="header-sticky">
            <div class="container">
                <div class="header-inner">
                    <div class="row">
                        <div class="col-xs-1 col-xxs-12"></div>
                        <div class="col-lg-7 col-md-6 col-sm-5 col-xs-2 col-xxs-3">
                            <div class="header-left">
                                <div class="header-main-menu">
                                    <ul class="main-menu"></ul>
                                </div>
                                <div class="header-mobile-menu">
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <span class="text">Main Menu</span>
                                        <span class="icon"><span></span><span></span><span></span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-5 col-xs-8 col-xxs-9">
                            <div class="header-right">
                                <div class="header-search style-1">
                                    <form method="get" class="search-form">
                                        <label><input type="text" name="search" class="input" placeholder="Search entire store here"></label>
                                        <button type="submit" class="button"><span class="fa fa-search"></span></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>