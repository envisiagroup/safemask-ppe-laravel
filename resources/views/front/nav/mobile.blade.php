<!-- header mobile nav -->
        <div class="header-mobile-nav is-sticky">
            <div class="container">
                <div class="mobile-inner">
                    <div class="logo">
                        <a href="index.html"><figure><img src="assets/images/logo-mobile.png" alt=""></figure></a>
                    </div>
                    <div class="mobile-control">
                        <div class="header-mobile-menu">
                            <a href="javascript:void(0)" class="menu-toggle">
                                <span class="text">Main Menu</span>
                                <span class="icon"><span></span><span></span><span></span></span>
                            </a>
                        </div>
                        <div class="header-search style-1 envy-dropdown">
                            <a href="javascript:void(0)" data-envy="envy-dropdown" class="link-dropdown">
                                <span class="fa fa-search"></span>
                            </a>
                            <div class="sub-menu">
                                <form method="get" class="search-form">
                                    <label><input type="text" name="search" class="input" placeholder="Search entire store here"></label>
                                    <button type="submit" class="button"><span class="fa fa-search"></span></button>
                                </form>
                            </div>
                        </div>
                        <div class="header-cart envy-dropdown">
                            <a href="javascript:void(0)" data-envy="envy-dropdown" class="link-dropdown">
                                <span class="icon fa fa-shopping-basket"></span>
                                <span class="text">Bag</span>
                                <span class="count">3</span>
                                <span class="total">- $90.00</span>
                            </a>
                            <div class="sub-menu">
                                <div class="cart-head">
                                    <h3 class="cart-title">You have <span>3 items</span> in your bag</h3>
                                </div>
                                <div class="cart-content">
                                    <ul class="cart-list">
                                        <li class="cart-item">
                                            <a href="product.html" class="thumb"><figure><img src="assets/images/product-01.jpg" alt=""></figure></a>
                                            <h3 class="title"><a href="product.html">Rustic Suit Blazer New</a></h3>
                                            <span class="price">$30.00</span>
                                            <span class="quantity">(x2)</span>
                                            <p class="remove"><a href="#"><span class="fa fa-times"></span></a></p>
                                        </li>
                                        <li class="cart-item">
                                            <a href="product.html" class="thumb"><figure><img src="assets/images/product-11.jpg" alt=""></figure></a>
                                            <h3 class="title"><a href="product.html">Rustic Suit Blazer New</a></h3>
                                            <span class="price">$30.00</span>
                                            <span class="quantity">(x1)</span>
                                            <p class="remove"><a href="#"><span class="fa fa-times"></span></a></p>
                                        </li>
                                    </ul>
                                    <div class="cart-total">
                                        <p>TOTAL PRICE <span>$90.00</span></p>
                                    </div>
                                    <div class="cart-action">
                                        <a href="cart.html" class="button cart">YOUR SHOPPING BAG</a>
                                        <a href="checkout.html" class="button checkout">CHECK OUT</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>