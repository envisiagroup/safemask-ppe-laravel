    <div class="footer-bottom">
        <div class="container">
            <div class="footer-inner">
                <div class="row">
                    <div class="col-md-7 col-sm-12 custom-5">
                        <div class="envy-menu style-2">
                            <ul>
                                <li><a href="/">Home</a></li>
                                <li><a href="/legal/terms">Terms of Use</a></li>
                                <li><a href="/legal/privacy">Privacy Policy</a></li>
                                <li>© {{ now()->year }} {{ config('envisia.site.title') }}. All Rights Reserved.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 custom-6">
                        <div class="envy-gallery style-1">
                            <ul>
                                <li class="effect bounce-in"><figure><img src="/assets/images/payment-01.png" alt=""></figure></li>
                                <li class="effect bounce-in"><figure><img src="/assets/images/payment-02.png" alt=""></figure></li>
                                <li class="effect bounce-in"><figure><img src="/assets/images/payment-03.png" alt=""></figure></li>
                                <li class="effect bounce-in"><figure><img src="/assets/images/payment-04.png" alt=""></figure></li>
                                <li class="effect bounce-in"><figure><img src="/assets/images/payment-05.png" alt=""></figure></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
