    <!-- header main -->
            <div class="header-bottom">
                <div class="container">
                    <div class="header-inner mega-menu-wapper">
                        <div class="row">
                            <div class="col-xs-1 col-xxs-12"></div>
                            <div class="col-lg-7 col-md-6 col-sm-5 col-xs-2 col-xxs-3">
                                <div class="header-left">
                                    <div class="header-main-menu">
                                        <ul class="main-menu envy-clone-mobile-menu envy-clone-sticky-menu">
                                            <li class="menu-item menu-item-has-children current-menu-item parent-menu-item">
                                                <a href="/" class="envy-menu-item-title">Home</a>
                                            </li>
                                            <li class="menu-item menu-item-has-children">
                                                <a href="/blog" class="envy-menu-item-title">Blog</a>
                                            </li>
                                            <li class="menu-item menu-item-has-children">
                                                <a href="/products" class="envy-menu-item-title">Products</a>
                                            </li>
                                            <li class="menu-item menu-item-has-children">
                                                <a href="/about" class="envy-menu-item-title">About</a>
                                            </li>
                                            <li class="menu-item menu-item-has-children">
                                                <a href="/contact" class="envy-menu-item-title">Contact</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="header-mobile-menu">
                                        <a href="javascript:void(0)" class="menu-toggle">
                                            <span class="text">Main Menu</span>
                                            <span class="icon"><span></span><span></span><span></span></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-8 col-xxs-9">
                                <div class="header-right">
                                    <div class="header-search style-1">
                                        <form method="get" class="search-form">
                                            <label><input type="text" name="search" class="input" placeholder="Search entire store here"></label>
                                            <button type="submit" class="button"><span class="fa fa-search"></span></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        