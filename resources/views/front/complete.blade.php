@extends('front.layouts.app')

@section('pixels')
<script>
  fbq('track', 'Purchase', {
    value: {{ $total }},
    currency: 'USD',
  });
</script>
@endsection

@section('content')
<!-- HERO BANNER -->
<section class="hero-banner small background no-padding">
    <div class="floral-pattern" data-stellar-background-ratio="0.8"></div>
    <span class="triangle triangle--top-left-small" style="border-width: 50px 0px 0px 1388px;"></span>
</section>
<!-- END HERO BANNER -->

<!-- Checkout -->
<section class="cart-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="product-title">
                    <h3 class="moist-heading text-center no-padding-top no-after"><span>Thank You</span></h3>
                    <h1 class="text-center page-title">Order #{{ $orderId }}</h1>
                </div>
            </div>
        </div><!-- END Row -->
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <p>Thank you for your order! It is being processed and will be shipped within 1 business day. You will receive an email with a tracking number once it has been shipped.</p>
                <p>If you have any questions or concerns, please call our customer support at (866) 517-5368.</p>
                <a href="/products" class="btn btn-default">Continue Shopping</a>
            </div>
        </div><!-- END Row -->
    </div>
</section>
@endsection
