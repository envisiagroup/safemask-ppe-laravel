@extends('front.layouts.app')

@section('content')

<!--MAIN CONTAINER-->
        <div class="main-section inner-page-about">

            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="index.html"><span>Home</span></a></li>
                    <li><span>About Us</span></li>
                </ul>
            </div>

            <!--SECTION 01-->
            <div id="section-1">
                <div class="container">
                    <div class="section-inner">
                        <div class="row">
                            <div class="col-md-6 col-xs-12">
                                <div class="envy-image">
                                    <div class="image-wrap effect plus-zoom"><figure><img src="assets/images/about-us-image-01.jpg" alt=""></figure></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <h3 class="envy-heading style-2 first-elm">Who we are</h3>
                                <p class="envy-heading style-3">Envy is an emerging leader in the luxury fashion business, bringing a fresh approach to the category through fashion-forward merchandise selection, sophisticated store design and exceptional customer care.  The name, taken from the Golbal word "Envynaly" meaning daily, is a nod to the belief that wearing lovely fashion is an everyday luxury.  Envy's mission is to help people feel beautiful from the inside out....starting at the underpinnings.</p>
                                <h3 class="envy-heading style-2">What we do</h3>
                                <p class="envy-heading style-3">Envy is run by a tight-knit group of talented, ambitious women and men, united by our passion (or maybe it's an obsession?) for gorgeous fashion. It's simple: We love what we do, as well as who we sit next to. We thrive on our collective dreams for the company, the joy of teamwork, and our (seriously) fun work environment.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--SECTION 02-->
            <div id="section-2">
                <div class="container">
                    <div class="section-inner">
                        <h2 class="envy-heading style-4 text-center"><span>ENVY’S NUMBER TRAFFICS</span></h2>
                        <p class="envy-heading style-5 text-center">We have 100+ Million people visits each month & Over 10+ thousand email Subscribe.</p>
                        <div class="envy-slider style-1">
                            <div class="slick-slider" data-slick='{"arrows":true,"slidesMargin":30,"infinite":true,"speed":1000,"autoplay":true,"autoplaySpeed":1500,"slidesToShow":3,"rows":1}' data-responsive='[{"breakpoint":1200,"settings":{"slidesToShow":3}},{"breakpoint":992,"settings":{"slidesToShow":2}},{"breakpoint":768,"settings":{"slidesToShow":2}},{"breakpoint":480,"settings":{"slidesToShow":1}}]'>
                                <div class="envy-statistic style-1">
                                    <div class="statistic-inner">
                                        <div class="content">
                                            <div class="thumb">
                                                <figure><img src="assets/images/statistic-01.png" alt=""></figure>
                                            </div>
                                            <div class="info">
                                                <span class="icon fa fa-instagram"></span>
                                                <span class="number">36m</span>
                                                <span class="description">Followers</span>
                                            </div>
                                        </div>
                                        <h4 class="title"><a href="#">Follow Us</a></h4>
                                    </div>
                                </div>
                                <div class="envy-statistic style-1">
                                    <div class="statistic-inner">
                                        <div class="content">
                                            <div class="thumb">
                                                <figure><img src="assets/images/statistic-02.png" alt=""></figure>
                                            </div>
                                            <div class="info">
                                                <span class="icon fa fa-facebook"></span>
                                                <span class="number">31m</span>
                                                <span class="description">Likes</span>
                                            </div>
                                        </div>
                                        <h4 class="title"><a href="#">Like Us</a></h4>
                                    </div>
                                </div>
                                <div class="envy-statistic style-1">
                                    <div class="statistic-inner">
                                        <div class="content">
                                            <div class="thumb">
                                                <figure><img src="assets/images/statistic-03.png" alt=""></figure>
                                            </div>
                                            <div class="info">
                                                <span class="icon fa fa-twitter"></span>
                                                <span class="number">61m</span>
                                                <span class="description">Followers</span>
                                            </div>
                                        </div>
                                        <h4 class="title"><a href="#">Follow Us</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--SECTION 03-->
            <div id="section-3">
                <div class="container">
                    <div class="section-inner">
                        <h2 class="envy-heading style-4 text-center"><span>WHAT OUR CLIENTS SAY</span></h2>
                        <div class="envy-slider style-1">
                            <div class="slick-slider" data-slick='{"arrows":true,"slidesMargin":30,"infinite":true,"speed":1000,"autoplay":true,"autoplaySpeed":1500,"slidesToShow":1,"rows":1}'>
                                <div class="envy-client style-1">
                                    <p class="text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac felis vel erat faucibus venenatis sit amet quis ante. Aliquam turpis magna cursus a egestas commodo, placerat a urna.”</p>
                                    <h4 class="author">
                                        <a href="#">Jimmy Hoang</a>
                                    </h4>
                                </div>
                                <div class="envy-client style-1">
                                    <p class="text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ac felis vel erat faucibus venenatis sit amet quis ante. Aliquam turpis magna cursus a egestas commodo, placerat a urna.”</p>
                                    <h4 class="author">
                                        <a href="#">Jimmy Hoang</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    @endsection