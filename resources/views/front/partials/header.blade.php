@include('front.nav.mobile')
@include('front.nav.sticky')
<!-- HEADER -->
<header id="header" class="header style-1">
	@include('front.nav.top')
	<!-- header middle -->
    <div class="header-middle">
        <div class="container">
            <div class="header-inner">
                <div class="header-left">
                    <div class="header-socials envy-socials style-1">
                        <ul>
                            <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                            <li><a href="#"><span class="fa fa-youtube-play"></span></a></li>
                            <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="header-center">
                    <div class="logo">
                        <a href="/"><figure><img src="{{ asset(config('envisia.site.logo')) }}" alt="logo" width="250"></figure></a>
                    </div>
                </div>
                <div class="header-right">
                    <div class="header-cart envy-dropdown">
                        <a href="javascript:void(0)" data-envy="envy-dropdown" class="link-dropdown">
                            <span class="icon fa fa-shopping-cart"></span>
                            <span class="text">Cart</span>
                            <span class="count">3</span>
                            <span class="total">- ${{ \Cart::subtotal() }}</span>
                        </a>
                        <div class="sub-menu">
                            <div class="cart-head">
                                <h3 class="cart-title">You have <span>3 items</span> in your bag</h3>
                            </div>
                            <div class="cart-content">
                                <ul class="cart-list">
                                    <li class="cart-item">
                                        <a href="product.html" class="thumb"><figure><img src="{{ asset('assets/images/product-01.jpg') }}" alt=""></figure></a>
                                        <h3 class="title"><a href="product.html">Rustic Suit Blazer New</a></h3>
                                        <span class="price">$30.00</span>
                                        <span class="quantity">(x2)</span>
                                        <p class="remove"><a href="#"><span class="fa fa-times"></span></a></p>
                                    </li>
                                    <li class="cart-item">
                                        <a href="product.html" class="thumb"><figure><img src="{{ asset('assets/images/product-11.jpg') }}" alt=""></figure></a>
                                        <h3 class="title"><a href="product.html">Rustic Suit Blazer New</a></h3>
                                        <span class="price">$30.00</span>
                                        <span class="quantity">(x1)</span>
                                        <p class="remove"><a href="#"><span class="fa fa-times"></span></a></p>
                                    </li>
                                </ul>
                                <div class="cart-total">
                                    <p>TOTAL PRICE <span>$90.00</span></p>
                                </div>
                                <div class="cart-action">
                                    <a href="/cart" class="button cart">YOUR SHOPPING BAG</a>
                                    <a href="/checkout" class="button checkout">CHECK OUT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('front.nav.main')
</header>
