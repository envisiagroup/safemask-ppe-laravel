    <!-- ToTop Button-->
    <a href="#" class="backtotop"><span class="fa fa-angle-up"></span></a>
    <!-- footer  -->
    <div class="footer-middle">
        <div class="container">
            <div class="footer-inner">
                <div class="row">
                    <div class="col-md-2 col-sm-4 custom-1">
                        <h3 class="envy-heading style-1">HOW CAN WE HELP?</h3>
                        <div class="envy-menu style-1">
                            <ul>
                                <li><a href="#">Need Help</a></li>
                                <li><a href="#">Track My Order</a></li>
                                <li><a href="#">Customer Services Twitter</a></li>
                                <li><a href="#">Returns & exchange</a></li>
                                <li><a href="#">Size Guide</a></li>
                                <li><a href="#">Student Discount</a></li>
                                <li><a href="#">Gift Vouchers</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 custom-2">
                        <h3 class="envy-heading style-1">INFOMATION</h3>
                        <div class="envy-menu style-1">
                            <ul>
                                <li><a href="#">Delivery Information</a></li>
                                <li><a href="#">Exclusive Offers & Updates</a></li>
                                <li><a href="#">Accessibility</a></li>
                                <li><a href="#">Privacy Notice</a></li>
                                <li><a href="#">About Cookies</a></li>
                                <li><a href="#">Submit Your Look</a></li>
                                <li><a href="#">FAQs</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-4 custom-3">
                        <h3 class="envy-heading style-1">ABOUT US</h3>
                        <div class="envy-menu style-1">
                            <ul>
                                <li><a href="#">About Envy</a></li>
                                <li><a href="#">Investor Relations</a></li>
                                <li><a href="#">Envy Social Responsibility</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Become an Affiliate</a></li>
                                <li><a href="#">Become a Partner</a></li>
                                <li><a href="#">Sitemap</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 custom-4">
                        <h3 class="envy-heading style-1">GET EXCLUSIVE OFFERS & UPDATES</h3>
                        <div class="envy-newsletter style-1">
                            <div class="newsletter-inner">
                                <div class="newsletter-form-wrap">
                                    <label class="text-field">
                                        <input type="email" name="email" class="input email" placeholder="Enter your email address">
                                    </label>
                                    <a href="#" class="button">SIGN UP</a>
                                </div>
                            </div>
                        </div>
                        <div class="envy-socials style-1">
                            <ul>
                                <li><a href="#"><span class="fa fa-pinterest-p"></span></a></li>
                                <li><a href="#"><span class="fa fa-youtube-play"></span></a></li>
                                <li><a href="#"><span class="fa fa-instagram"></span></a></li>
                                <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                                <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    