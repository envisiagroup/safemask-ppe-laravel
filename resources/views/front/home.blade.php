    @extends('front.layouts.app')

    @section('content')

    @include('front.slider.home')
    <div id="section-2">
        <div class="container">
            <div class="section-inner">
                <div class="envy-slider style-1">
                    <div class="slick-slider" data-slick='{"slidesToShow":3,"slidesMargin":30}' data-responsive='[{"breakpoint":992,"settings":{"slidesToShow":2}},{"breakpoint":768,"settings":{"slidesToShow":1, "arrows":false}}]'>
                        <div class="envy-category style-2">
                            <div class="category-inner">
                                <div class="thumb">
                                    <a href="#"><figure><img src="assets/images/category-06.jpg" alt=""></figure></a>
                                </div>
                                <div class="content">
                                    <h3 class="title">LADY'S CLOTHING</h3>
                                    <a href="#" class="button">SHOP NOW <span class="fa fa-caret-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="envy-category style-2">
                            <div class="category-inner">
                                <div class="thumb">
                                    <a href="#"><figure><img src="assets/images/category-07.jpg" alt=""></figure></a>
                                </div>
                                <div class="content">
                                    <h3 class="title">MEN'S CLOTHING</h3>
                                    <a href="#" class="button">SHOP NOW <span class="fa fa-caret-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="envy-category style-2">
                            <div class="category-inner">
                                <div class="thumb">
                                    <a href="#"><figure><img src="assets/images/category-08.jpg" alt=""></figure></a>
                                </div>
                                <div class="content">
                                    <h3 class="title">ENVY'S ACCESSORIES</h3>
                                    <a href="#" class="button">SHOP NOW <span class="fa fa-caret-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection